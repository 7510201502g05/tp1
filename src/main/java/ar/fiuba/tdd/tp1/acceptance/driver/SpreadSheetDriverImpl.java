package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.command.CommandExecutor;
import ar.fiuba.tdd.tp1.command.CommandExecutorImpl;
import ar.fiuba.tdd.tp1.command.ModifySpreadSheetCommand;
import ar.fiuba.tdd.tp1.evaluator.impl.CycleDetectedException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.exporter.BookExporter;
import ar.fiuba.tdd.tp1.exporter.CsvExporter;
import ar.fiuba.tdd.tp1.exporter.JsonExporter;
import ar.fiuba.tdd.tp1.exporter.SheetExporter;
import ar.fiuba.tdd.tp1.importer.BookImporter;
import ar.fiuba.tdd.tp1.importer.CsvImporter;
import ar.fiuba.tdd.tp1.importer.JsonImporter;
import ar.fiuba.tdd.tp1.importer.SheetImporter;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.ContentAndFormatterSpreadSheetImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Es el SpreadSheet que maneja formatos y valores. Tambien permite pesistir los libros.
 * 
 * @author Galli
 *
 */
public class SpreadSheetDriverImpl implements SpreadSheetTestDriver {

    private Map<String, ContentAndFormatterSpreadSheet> workBooks;
    
    private String filePath = "tmp\\";

    private CommandExecutor commandExecutor;

    public SpreadSheetDriverImpl() {
        workBooks = new LinkedHashMap<String, ContentAndFormatterSpreadSheet>();
        commandExecutor = new CommandExecutorImpl();
    }
    
    /**
     * Setea la direccion donde se van a guardar los archivos.
     * 
     * @param filePath : direccion de los archivos exportados e importados.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public List<String> workBooksNames() {
        List<String> listNames = new ArrayList<String>();
        listNames.addAll(workBooks.keySet());
        return listNames;
    }

    @Override
    public void createNewWorkBookNamed(String workbookName) {
        if (workBooks.get(workbookName) != null) {
            throw new ExistingBookFoundException("El libro que se quiere crear ya existe.");
        }
        ContentAndFormatterSpreadSheet spreadSheet = new ContentAndFormatterSpreadSheetImpl();
        spreadSheet.createSheet("default");
        workBooks.put(workbookName, spreadSheet);
        commandExecutor.runCommand(new ModifySpreadSheetCommand<>(spreadSheet));
    }

    @Override
    public List<String> workSheetNamesFor(String bookName) {
        List<String> sheetNames = getBook(bookName).getNamesSheet();
        System.out.println(sheetNames);
        return sheetNames;
    }

    /**
     * Devuelve el book que se esta buscando.
     * 
     * @param bookName
     *            : nombre del book que se busca.
     * @return devuelve el book deseado.
     */
    private ContentAndFormatterSpreadSheet getBook(String bookName) {
        ContentAndFormatterSpreadSheet book = workBooks.get(bookName);
        if (book == null) {
            throw new BookNotFoundException("El libro que se intenta acceder no existe.");
        }
        return book;
    }

    @Override
    public void createNewWorkSheetNamed(String bookName, String sheetName) {
        ContentAndFormatterSpreadSheet spreadSheet = getBook(bookName);
        spreadSheet.createSheet(sheetName);
        workBooks.put(bookName, spreadSheet);
        commandExecutor.runCommand(new ModifySpreadSheetCommand<>(spreadSheet));
    }

    /**
     * Elimina la hoja de un libro.
     * 
     * @param bookName
     *            : nombre del libro.
     * @param sheetName
     *            : nombre de la hoja.
     */
    public void removeWorkSheetNamed(String bookName, String sheetName) {
        ContentAndFormatterSpreadSheet spreadSheet = getBook(bookName);
        spreadSheet.removeSheet(sheetName);
        commandExecutor.runCommand(new ModifySpreadSheetCommand<>(spreadSheet));
    }

    /**
     * Transforma una expresion del tipo !hoja.C23 al tipo hoja!C23 que nuestra API entiende,
     * 
     * @param expression
     *            : Expresion a transformar.
     * @return Expresion transformada.
     */
    private String transformInputExpression(String expression) {
        String regex = "!\\w+\\.[A-Z]+\\d+";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(expression);
        while (matcher.find()) {
            String sequence = matcher.group();
            String[] split = sequence.split("\\.");
            String sheet = split[0].replaceAll("!", "");
            String cell = split[1];
            String trueReference = sheet + "!" + cell;
            expression = expression.replaceAll(sequence, trueReference);
        }

        return expression;
    }

    @Override
    public void setCellValue(String bookName, String sheetName, String colRow, String rawValue) {
        rawValue = transformInputExpression(rawValue);
        CrPair crpair = createCrPair(colRow);
        getBook(bookName).putCellRawValue(sheetName, crpair.getColumn(), crpair.getRow(), rawValue);
        runModifyContentAndFormatterSpreadSheetCommand(bookName);
    }

    /**
     * Cambia el formato de una celda a String.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatString(String bookName, String sheet, String colRow)
            throws InvalidCellIdentifierException {
        CrPair crpair = createCrPair(colRow);
        getBook(bookName).putCellFormatString(sheet, crpair.getColumn(), crpair.getRow());
        runModifyContentAndFormatterSpreadSheetCommand(bookName);
    }

    /**
     * Cambia el formato de una celda a Numero.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param decimal
     *            : es la cantidad de decimales.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatNumber(String bookName, String sheet, String colRow, int decimal)
            throws InvalidCellIdentifierException {
        CrPair crpair = createCrPair(colRow);
        getBook(bookName).putCellFormatNumber(sheet, crpair.getColumn(), crpair.getRow(), decimal);
        runModifyContentAndFormatterSpreadSheetCommand(bookName);
    }

    /**
     * Cambia el formato de una celda a Moneda.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param symbol
     *            : es el simbolo de la moneda que selecciono el usuario.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatMoney(String bookName, String sheet, String colRow, String symbol)
            throws InvalidCellIdentifierException {
        CrPair crpair = createCrPair(colRow);
        getBook(bookName).putCellFormatMoney(sheet, crpair.getColumn(), crpair.getRow(), symbol);
        runModifyContentAndFormatterSpreadSheetCommand(bookName);
    }

    /**
     * Cambia el formato de una celda a Fecha.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param dateOrder
     *            : es orden del a�o, mes y dia.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatDate(String bookName, String sheet, String colRow, String dateOrder)
            throws InvalidCellIdentifierException {
        CrPair crpair = createCrPair(colRow);
        getBook(bookName).putCellFormatDate(sheet, crpair.getColumn(), crpair.getRow(), dateOrder);
        runModifyContentAndFormatterSpreadSheetCommand(bookName);
    }

    /**
     * Crea y ejecuta un comando para modificar el spreadSheet
     * 
     * @param bookName
     *            : es libro que se modifico.
     */
    private void runModifyContentAndFormatterSpreadSheetCommand(String bookName) {
        commandExecutor.runCommand(new ModifySpreadSheetCommand<>(workBooks.get(bookName)));
    }

    /**
     * Crea un CrPair a partir de la posicion de la celda (columna+fila).
     * 
     * @param colRow
     *            : columna+fila de una celda.
     * @return una CrPair de esa posicion.
     */
    private CrPair createCrPair(String colRow) {
        return CrPair.create(colRow);
    }

    @Override
    public double getCellValueAsDouble(String bookName, String sheetName, String colRow) {
        try {
            return Double.valueOf(this.getCellValueAsString(bookName, sheetName, colRow));
        } catch (CycleDetectedException cd) {
            throw new BadReferenceException();
        } catch (SheetNotFoundException exc) {
            throw new UndeclaredWorkSheetException("La hoja que se intenta acceder no existe");
        } catch (Exception ex) {
            throw new BadFormulaException(ex);
        }
    }

    /**
     * Retorna el valor de una celda como un String.
     * 
     * @param bookName
     *            - Nombre del libro.
     * @param sheetName
     *            - Nombre de la hoja.
     * @param colRow
     *            - ColumnaFila (A23).
     * @return valor de una celda como un String.
     */
    public String getCellValueAsStringWitoutFormat(String bookName, String sheetName, String colRow) {
        CrPair crpair = CrPair.create(colRow);
        ContentAndFormatterSpreadSheet spreadSheet = getBook(bookName);
        try {
            return spreadSheet.getCellEvaluatedValue(sheetName, crpair.getColumn(), crpair.getRow());
        } catch (SheetNotFoundException exc) {
            throw new UndeclaredWorkSheetException("La hoja que se intenta acceder no existe");
        }
    }

    @Override
    public String getCellValueAsString(String bookName, String sheetName, String colRow) {
        CrPair crpair = CrPair.create(colRow);
        ContentAndFormatterSpreadSheet spreadSheet = getBook(bookName);
        try {
            return spreadSheet.getCellFormattedValue(sheetName, crpair.getColumn(), crpair.getRow());
        } catch (SheetNotFoundException exc) {
            throw new UndeclaredWorkSheetException("La hoja que se intenta acceder no existe");
        }
    }

    @Override
    public void undo() {
        commandExecutor.undo();
    }

    @Override
    public void redo() {
        commandExecutor.redo();
    }

    /**
     * Devuelve un mapa con los valores crudos de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsInBook(String bookName) {
        return getBook(bookName).getCellsInBook();
    }

    /**
     * Devuelve un mapa con las celdas formateadas de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Cell<CrPair, String>>> getCellsFormattedInBook(
            String bookName) {
        return getBook(bookName).getCellsFormattedInBook();
    }

    /**
     * Devuelve un mapa con la informacion de los formatos de cada celda de un libro.
     * 
     * @param bookName
     *            : nombre del libro que se busca.
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Map<String, String>>> getFormatterInBook(String bookName) {
        return getBook(bookName).getFormatterInBook();
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId,
            String formatter, String format) {
        CrPair crpair = CrPair.create(cellId);
        this.getBook(workBookName).putCellFormatter(workSheetName, crpair.getColumn(),
                crpair.getRow(), formatter, format);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        // TODO NO MANEJAMOS TIPOS
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        BookExporter exporter = new JsonExporter();
        exporter.export(getCellsInBook(workBookName), getFormatterInBook(workBookName), filePath,
                fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String workBookName, String fileName) {
        BookImporter importer = new JsonImporter();
        importer.importFromFile(this, filePath, fileName, workBookName);
    }

    @Override
    public void saveAsCSV(String workBookName, String workSheetName, String fileName) {
        Map<CrPair, Cell<CrPair, String>> mapSheet = this.getCellsFormattedInBook(workBookName).get(workSheetName);
        SheetExporter exporter = new CsvExporter();
        exporter.export(mapSheet, filePath, fileName);
    }

    @Override
    public void loadFromCSV(String workBookName, String workSheetName, String fileName) {
        SheetImporter importer = new CsvImporter();
        importer.importFromFile(this, filePath, fileName, workBookName, workSheetName);
    }

    @Override
    public Integer sheetCountFor(String workBookName) {
        ContentAndFormatterSpreadSheet book = this.getBook(workBookName);
        return book.getNamesSheet().size();
    }

}
