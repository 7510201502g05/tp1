package ar.fiuba.tdd.tp1.acceptance.driver;

import java.util.List;

public interface SpreadSheetTestDriver {

    /**
     * Retorna una lista con los nombres de los libros creados.
     * 
     * @return lista con los nombres de los libros creados.
     */
    List<String> workBooksNames();

    /**
     * Crea un nuevo libro.
     * 
     * @param workbookName
     *            : Nombre del libro nuevo a crear.
     */
    void createNewWorkBookNamed(String name);

    /**
     * Crea una nueva hoja en un libro.
     * 
     * @param workbookName
     *            : nombre del libro.
     * @param name
     *            : nombre de la nueva hoja.
     */
    void createNewWorkSheetNamed(String workbookName, String name);

    /**
     * Devuelve una lista con los nombres de las hojas que tiene un libro.
     * 
     * @param bookName
     *            : nombre del libro que se quiere obtener sus hojas.
     * @return lista de strings con los nombres de las hojas.
     */
    List<String> workSheetNamesFor(String workBookName);

    /**
     * Establece el valor de una celda.
     * 
     * @param bookName
     *            : Nombre del libro.
     * @param sheetName
     *            : Nombre de la hoja.
     * @param colRow
     *            : ColumnaFila (A23).
     * @param rawValue
     *            : Valor a establecer.
     */
    void setCellValue(String workBookName, String workSheetName, String cellId, String value);

    /**
     * Devuelve el valor de una celda como el formato que se le habia asignado.
     * 
     * @param bookName
     *            : Nombre del libro.
     * @param sheetName
     *            : Nombre de la hoja.
     * @param colRow
     *            : ColumnaFila (A23).
     * @return valor de una celda como un String.
     */
    String getCellValueAsString(String workBookName, String workSheetName, String cellId);

    /**
     * Retorna el valor de una celda como un Double.
     * 
     * @param bookName
     *            - Nombre del libro.
     * @param sheetName
     *            - Nombre de la hoja.
     * @param colRow
     *            - ColumnaFila (A23).
     * @return valor de una celda como un Double.
     */
    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId);

    /**
     * Deshace la ultima modificacion que se hizo.
     */
    void undo();

    /**
     * Rehace la ultima modificacion que se deshizo.
     */
    void redo();

    /**
     * Pone un formato especifico a una celda de un hoja de un libro determinado.
     * 
     * @param workBookName
     *            : nombre del libro.
     * @param workSheetName
     *            : nombre de la hoja.
     * @param cellId
     *            : posicion de la celda (columna+fila).
     * @param formatter
     *            : el tipo de formato.
     * @param format
     *            : caracteristicas del formato.
     */
    void setCellFormatter(String workBookName, String workSheetName, String cellId,
            String formatter, String format);

    void setCellType(String workBookName, String workSheetName, String cellId, String type);

    /**
     * Persiste un libro en un archivo con formato Json.
     * 
     * @param workBookName
     *            : nombre del libro a persistir.
     * @param fileName
     *            : direccion del archivo.
     */
    void persistWorkBook(String workBookName, String fileName);

    /**
     * Importa un libro desde un archivo con formato Json.
     * 
     * @param workBookName
     *            : nombre del libro a persistir.
     * @param fileName
     *            : direccion del archivo.
     */
    void reloadPersistedWorkBook(String workBookName, String fileName);

    void saveAsCSV(String workBookName, String workSheetName, String fileName);

    void loadFromCSV(String workBookName, String workSheetName, String fileName);

    Integer sheetCountFor(String string);

}
