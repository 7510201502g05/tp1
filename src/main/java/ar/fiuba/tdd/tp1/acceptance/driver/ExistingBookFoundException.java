package ar.fiuba.tdd.tp1.acceptance.driver;

public class ExistingBookFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExistingBookFoundException(String msg) {
        super(msg);
    }
}
