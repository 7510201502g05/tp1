package ar.fiuba.tdd.tp1.controller;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.command.CommandExecutor;
import ar.fiuba.tdd.tp1.command.CommandExecutorImpl;
import ar.fiuba.tdd.tp1.command.ModifyCurrentSheetAndBook;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exporter.CsvExporter;
import ar.fiuba.tdd.tp1.exporter.SheetExporter;
import ar.fiuba.tdd.tp1.importer.CsvImporter;
import ar.fiuba.tdd.tp1.importer.SheetImporter;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.parser.var.ColumnSequence;
import ar.fiuba.tdd.tp1.parser.var.NestedColumnSequence;
import ar.fiuba.tdd.tp1.window.table.TableContainer;
import ar.fiuba.tdd.tp1.window.table.WindowTable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.swing.table.DefaultTableModel;

/**
 * Esta clase se encarga de guardar todos los datos del SpreadSheet y permite setearle los valores
 * de una hoja a una TableModel.
 * 
 * @author Galli
 *
 */
public class ControllerWindow implements TableContainer, ControllerSpreadSheet,
        SetterCurrentSheetAndBook {

    private SpreadSheetDriverImpl spreadSheet;
    private CommandExecutor commandExecutor;
    private String sheetNameModified;
    private String bookNameModified;
    private int numColumn;
    private static final int numberMinColumn = 14;
    private static final String minColumn = "O"; // 14 despues de la A
    private static final int numberMinRow = 14;

    private WindowTable frame = null;

    public ControllerWindow() {
        spreadSheet = new SpreadSheetDriverImpl();
        commandExecutor = new CommandExecutorImpl();
    }

    @Override
    public void createNewWorkBookNamed(String workbookName) {
        spreadSheet.createNewWorkBookNamed(workbookName);
    }

    @Override
    public void createNewWorkSheetNamed(String bookName, String sheetName) {
        spreadSheet.createNewWorkSheetNamed(bookName, sheetName);
        saveNamesAndPrintTable(bookName, sheetName);
    }

    @Override
    public void removeWorkSheetNamed(String bookName, String sheetName) {
        spreadSheet.removeWorkSheetNamed(bookName, sheetName);
    }

    /**
     * Guarda los nombres del libro y la hoja que fueron modificados e imprime la tabla.
     * 
     * @param bookName
     *            : nombre del libro modificado.
     * @param sheetName
     *            : nombre de la hoja modificada.
     */
    private void saveNamesAndPrintTable(String bookName, String sheetName) {
        commandExecutor.runCommand(new ModifyCurrentSheetAndBook(bookName, sheetName,
                bookNameModified, sheetNameModified, this));
        printTable();
    }

    @Override
    public void setCellValueInTheCurrentSheetAndBook(String colRow, String rawValue) {
        spreadSheet.setCellValue(bookNameModified, sheetNameModified, colRow, rawValue);
        saveNamesAndPrintTable(bookNameModified, sheetNameModified);
    }

    @Override
    public void setCellValue(String bookName, String sheetName, String colRow, String rawValue) {
        spreadSheet.setCellValue(bookName, sheetName, colRow, rawValue);
        saveNamesAndPrintTable(bookName, sheetName);
    }

    @Override
    public void setCellFormatString(String bookName, String sheet, String colRow)
            throws InvalidCellIdentifierException {
        spreadSheet.setCellFormatString(bookName, sheet, colRow);
        saveNamesAndPrintTable(bookName, sheet);
    }

    @Override
    public void setCellFormatNumber(String bookName, String sheet, String colRow, int decimal)
            throws InvalidCellIdentifierException {
        spreadSheet.setCellFormatNumber(bookName, sheet, colRow, decimal);
        saveNamesAndPrintTable(bookName, sheet);
    }

    @Override
    public void setCellFormatMoney(String bookName, String sheet, String colRow, String symbol)
            throws InvalidCellIdentifierException {
        spreadSheet.setCellFormatMoney(bookName, sheet, colRow, symbol);
        saveNamesAndPrintTable(bookName, sheet);
    }

    @Override
    public void setCellFormatDate(String bookName, String sheet, String colRow, String dateOrder)
            throws InvalidCellIdentifierException {
        spreadSheet.setCellFormatDate(bookName, sheet, colRow, dateOrder);
        saveNamesAndPrintTable(bookName, sheet);
    }

    @Override
    public void printTable() {
        if (frame == null) {
            frame = new WindowTable(this);
        } else {
            frame.changeModel(this);
        }
        frame.printWindow();
    }

    @Override
    public void undo() {
        spreadSheet.undo();
        commandExecutor.undo();
        printTable();
    }

    @Override
    public void redo() {
        spreadSheet.redo();
        commandExecutor.redo();
        printTable();
    }

    @Override
    public DefaultTableModel addDataToTableModel(DefaultTableModel dtm) {
        dtm = addColumnsToTableModel(dtm);
        return addCellDataToTableModel(dtm);
    }
    
    
    private DefaultTableModel addCellDataToTableModel(DefaultTableModel dtm) {
        Map<CrPair, Cell<CrPair, String>> mapSheet = getCellsFormattedInBookInSheet();
        int numberRow = minRows(maxRow(mapSheet.keySet()));
        
        //System.out.println("numberColumn para agregar tabla: " + numColumn) ; //numberColumn);
        //System.out.println("numberRow para agregar tabla: " + numberRow);
        
        initializeTable(dtm, numColumn, numberRow); //numberColumn, numberRow);
        mapSheet.forEach((id, cell) -> {
                // El -1 es porque la numeracion de las filas en el set arranca en el 0
                dtm.setValueAt(cell.getRawValue(), id.getRow() - 1, columnNumber(id.getColumn()));
            });
        return dtm;
    }
    
    /**
     * Agrega al dtm las columnas que debe tener la tabla.
     * 
     * @param dtm
     *            : modelo de tabla.
     * @return dtm modificado.
     */
    private DefaultTableModel addColumnsToTableModel(DefaultTableModel dtm) {
        dtm.addColumn(bookNameModified + "." + sheetNameModified);
        dtm.addColumn("A");
        numColumn = 2;
        Map<CrPair, Cell<CrPair, String>> mapSheet = getCellsFormattedInBookInSheet();
        String maxColumn = lastColumn(maxColumn(mapSheet.keySet()));
        ColumnSequence columnSequence = NestedColumnSequence.create("A");
        while (!columnSequence.next().getCurrent().equals(maxColumn)) {
            dtm.addColumn(columnSequence.getCurrent());
            numColumn += 1;
        }
        return dtm;
    }

    /**
     * Devuelve el valor minimo de filas que se deben setear en una TableModel.
     * 
     * @param numberRow
     *            : es la fila mas alta que esta ocupada en un hoja.
     * @return el valor minimo de las filas.
     */
    private int minRows(int numberRow) {
        if (numberRow < numberMinRow) {
            return numberMinRow;
        }
        return numberRow;
    }

    /**
     * Devuelve el valor minimo de columnas que se deben setear en una TableModel.
     * 
     * @param numberRow
     *            : es la fila mas alta que esta ocupada en un hoja.
     * @return el valor minimo de las filas.
     */
    private int minColumns(int numberColumn) {
        if (numberColumn < numberMinColumn) {
            return numberMinColumn;
        }
        return numberColumn;
    }

    /**
     * Inicializa la tabla con "" en cada celda.
     * 
     * @param dtm
     *            : es la TableModel a la que se le debe insertar los valores.
     * @param columnNumber
     *            : es la ultima columna que se debe llenar.
     * @param rowNumber
     *            : es la ultima fila que se debe llenar.
     */
    private void initializeTable(DefaultTableModel dtm, int columnNumber, int rowNumber) {
        Object[] table = new Object[columnNumber];
        for (int i = 0; i < rowNumber; i++) {
            table[0] = i + 1; // Ya que las filas arranca desde el numero 1
            for (int j = 1; j < columnNumber; j++) {
                table[j] = "";
            }
            dtm.addRow(table);
        }
    }

    /**
     * Devuelve la fila mas alta que esta ocupada.
     * 
     * @param ids
     *            : son las celdas que hay que inspeccionar.
     * @return la fila mas alta.
     */
    private int maxRow(Set<CrPair> ids) {
        int max = 0;
        Iterator<CrPair> iterator = ids.iterator();
        while (iterator.hasNext()) {
            int row = iterator.next().getRow();
            if (row > max) {
                max = row;
            }
        }
        return max;
    }

    /**
     * Devuelve la posicion de la columna pasada por parametro.
     * 
     * @param maxColumn
     *            : es la columna que se quiere saber en que posicion esta.
     * @return la posicion de columna dentro de una tabla.
     */
    private int columnNumber(String maxColumn) {
        int cant = 1; // La primer columna es solo para los nombres de las filas
        ColumnSequence columnSequence = NestedColumnSequence.create("A");
        if (maxColumn.equals("A")) {
            return cant;
        }
        while (!columnSequence.next().getCurrent().equals(maxColumn)) {
            cant++;
        }
        cant++;
        return cant;
    }

    /**
     * Devuelve la columna mas alta que esta ocupada.
     * 
     * @param ids
     *            : conjunto de celdas ocupadas.
     * @return la columna mas alta.
     */
    private String maxColumn(Set<CrPair> ids) {
        String max = "A";
        Iterator<CrPair> iterator = ids.iterator();
        while (iterator.hasNext()) {
            String column = iterator.next().getColumn();
            if (column.compareTo(max) > 0) {
                max = column;
            }
        }
        return max;
    }

    /**
     * Devuelve un mapa con las celdas y sus formatos de la hoja y el libro actual.
     * 
     * @return un mapa con las celdas y sus formatos
     */
    private Map<CrPair, Cell<CrPair, String>> getCellsFormattedInBookInSheet() {
        HashMap<String, Map<CrPair, Cell<CrPair, String>>> map = spreadSheet
                .getCellsFormattedInBook(this.bookNameModified);
        return map.get(sheetNameModified);
    }

    /**
     * Devuelve la columna minima que se debe setear en la tabla.
     * 
     * @param maxcolumn
     *            : es la columna mas alta que esta ocupada.
     * @return una columna.
     */
    private String lastColumn(String maxcolumn) {
        if (maxcolumn.compareTo(minColumn) < 0) {
            return minColumn;
        }
        return maxcolumn;
    }

    @Override
    public void setCurrentSheetAndBook(String newSheet, String newBook) {
        Optional.ofNullable(newBook).ofNullable(newSheet);
        sheetNameModified = newSheet;
        bookNameModified = newBook;
    }

    @Override
    public void openSheet(String bookName, String sheetName) {
        setCurrentSheetAndBook(sheetName, bookName);
        this.saveNamesAndPrintTable(bookName, sheetName);
    }

    @Override
    public void persistCurrentSheet(String fileName) {
//        Map<CrPair, Cell<CrPair, String>> mapSheet = getCellsFormattedInBookInSheet();
//        SheetExporter exporter = new CsvExporter();
//        exporter.export(mapSheet, filePath, fileName);
        spreadSheet.saveAsCSV(bookNameModified, sheetNameModified, fileName);
    }

    @Override
    public void exportBook(String book, String fileName) {
        spreadSheet.persistWorkBook(book, fileName);
    }

    @Override
    public void importBook(String book, String fileName) {
        spreadSheet.reloadPersistedWorkBook(book, fileName);
    }

    @Override
    public void importCVS(String book, String sheet, String fileName) {
//        SheetImporter importer = new CsvImporter();
//        importer.importFromFile(spreadSheet, filePath, fileName, book, sheet);
//        saveNamesAndPrintTable(book, sheet);
        spreadSheet.loadFromCSV(book, sheet, fileName);
    }

}
