package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de rehacer el ultimo cambio que se deshizo en el controller.
 * 
 * @author Galli
 *
 */
public class Redo extends MenuOption {

    public Redo() {
        numParameter = 1;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.redo();
    }

}
