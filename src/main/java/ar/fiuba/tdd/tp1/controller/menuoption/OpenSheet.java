package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de abrir la hoja de un libro del controller.
 * 
 * @author Galli
 *
 */
public class OpenSheet extends MenuOption {

    public OpenSheet() {
        numParameter = 3;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.openSheet(parameter[2], parameter[1]);
    }
}
