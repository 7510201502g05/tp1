package ar.fiuba.tdd.tp1.controller;

import ar.fiuba.tdd.tp1.controller.menuoption.CVSImporterOption;
import ar.fiuba.tdd.tp1.controller.menuoption.CreateNewWorkBook;
import ar.fiuba.tdd.tp1.controller.menuoption.CreateNewWorkSheet;
import ar.fiuba.tdd.tp1.controller.menuoption.CsvExporterOption;
import ar.fiuba.tdd.tp1.controller.menuoption.ExportBook;
import ar.fiuba.tdd.tp1.controller.menuoption.HelpOption;
import ar.fiuba.tdd.tp1.controller.menuoption.ImportBook;
import ar.fiuba.tdd.tp1.controller.menuoption.OpenSheet;
import ar.fiuba.tdd.tp1.controller.menuoption.Redo;
import ar.fiuba.tdd.tp1.controller.menuoption.RemoveWorkSheet;
import ar.fiuba.tdd.tp1.controller.menuoption.SetCellFormatDate;
import ar.fiuba.tdd.tp1.controller.menuoption.SetCellFormatMoney;
import ar.fiuba.tdd.tp1.controller.menuoption.SetCellFormatNumber;
import ar.fiuba.tdd.tp1.controller.menuoption.SetCellFormatString;
import ar.fiuba.tdd.tp1.controller.menuoption.SetCellValue;
import ar.fiuba.tdd.tp1.controller.menuoption.Undo;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Esta clase es la que se encarga de capturar por consola los comandos y ejecutarlos.
 * 
 * @author Galli
 *
 */
public class ControllerConsoleWindow {

    private Map<String, MenuOption> menu = new HashMap<String, MenuOption>();

    @SuppressWarnings("CPD-START")
    public ControllerConsoleWindow() {
        addOption("crearLibro", new CreateNewWorkBook());
        addOption("crearHojaEnLibro", new CreateNewWorkSheet());
        addOption("borrarHojaEnLibro", new RemoveWorkSheet());
        addOption("abrirUnaHoja", new OpenSheet());
        addOption("ponerValorALaCelda", new SetCellValue());
        addOption("ponerFormatoStringALaCelda", new SetCellFormatString());
        addOption("ponerFormatoNumeroALaCelda", new SetCellFormatNumber());
        addOption("ponerFormatoMonedaALaCelda", new SetCellFormatMoney());
        addOption("ponerFormatoFechaALaCelda", new SetCellFormatDate());
        addOption("exportarCsv", new CsvExporterOption());
        addOption("exportarLibro", new ExportBook());
        addOption("importarLibro", new ImportBook());
        addOption("importarCsv", new CVSImporterOption());
        addOption("deshacer", new Undo());
        addOption("rehacer", new Redo());
        addOption("help", new HelpOption());
    }

    @SuppressWarnings("CPD-END")
    /**
     * Agrega una opcion al menu.
     * 
     * @param command : es el nombre del comando.
     * @param option : es la opcion que ejecuta ese comando.
     */
    private void addOption(String command, MenuOption option) {
        menu.put(command, option);
    }

    /**
     * Pide que el usuario ingrese comandos y los ejecuta hasta que se le envie un "cerrar".
     */
    @SuppressWarnings("resource")
    public void run() {
        ControllerWindow controller = new ControllerWindow();
        System.out.println("Ingrese los comandos:");
        Scanner in = new Scanner(System.in, "UTF-8");
        String command = in.nextLine();
        while (!command.equals("cerrar")) {
            String[] parameter = command.split(" ");
            MenuOption option = menu.get(parameter[0]);
            if (option == null) {
                System.out.println("El comando que se ingreso no es valido. Si desea "
                        + "saber que comandos son los permitidos ingrese el comando 'help'");
            } else {
                executeOption(option, parameter, controller);
            }
            System.out.println("Ingrese siguiente comando:");
            command = in.nextLine();
        }
    }

    /**
     * Ejecuta la opcion, si hubo algun inconveniente imprime por pantalla el error.
     * 
     * @param option
     *            : opcion que se quiere ejecutar.
     * @param parameter
     *            : parametros con lo que se quiere ejecutar una opcion.
     * @param controller
     *            : es el controlador donde se va a aplicar la ejecucion de la opcion.
     */
    private void executeOption(MenuOption option, String[] parameter, ControllerWindow controller) {
        try {
            option.execute(parameter, controller);
        } catch (Exception ife) {
            System.out.println(ife.getMessage());
        }
    }

}
