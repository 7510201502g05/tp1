package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de imprimir por consola todos los comandos que puede usar el usuario.
 * 
 * @author Galli
 *
 */
public class HelpOption extends MenuOption {

    public HelpOption() {
        this.numParameter = 1;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        System.out
                .println("Comandos:\n * crearLibro <nombre del libro>\n * crearHojaEnLibro "
                        + "<nombre de la hoja> <nombre del libro>\n * borrarHojaEnLibro <nombre de la hoja> "
                        + "<nombre del libro>\n * abrirUnaHoja <nombre de la hoja> <nombre del libro>\n * "
                        + "ponerValorALaCelda <nombre del libro> <nombre de la hoja> "
                        + "<columna y fila> <valor>\n * ponerFormatoStringALaCelda <nombre del libro> "
                        + "<nombre de la hoja> <columna y fila>\n * ponerFormatoNumeroALaCelda <nombre del libro> "
                        + "<nombre de la hoja> <columna y fila> <cantidad de decimales>\n * "
                        + "ponerFormatoMonedaALaCelda <nombre del libro> <nombre de la hoja> <columna y fila> "
                        + " <simbolo de la moneda>\n * ponerFormatoFechaALaCelda "
                        + "<nombre del libro> <nombre de la hoja> <columna y fila> <formato de la fecha>\n * "
                        + "exportarCsv <direccion del archivo> <nombre del archivo>\n * exportarLibro <nombre del libro> "
                        + "<nombre del archivo>\n * importarLibro <nombre del libro> <nombre del archivo>\n * importarCsv "
                        + "<nombre del libro> <nombre de la hoja> <direccion del archivo> <nombre del archivo>\n * "
                        + "deshacer\n * rehacer\n * cerrar\n\n\n Los formatos permitidos para la fecha son: DD-MM-YYYY,"
                        + " MM-DD-YYYY y YYYY-MM-DD\n\n");
    }

}
