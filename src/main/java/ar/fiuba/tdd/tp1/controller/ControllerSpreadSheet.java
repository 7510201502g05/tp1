package ar.fiuba.tdd.tp1.controller;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

public interface ControllerSpreadSheet {

    /**
     * Crea un nuevo libro.
     * 
     * @param workbookName
     *            - Nombre del libro nuevo a crear.
     */
    public void createNewWorkBookNamed(String workbookName);

    /**
     * Crea una nueva hoja para un determinado libro.
     * 
     * @param bookName
     *            : nombre del libro que se quiere agregar una hoja.
     * @param sheetName
     *            : nombre de la hoja que se quiere crear.
     */
    public void createNewWorkSheetNamed(String bookName, String sheetName);

    /**
     * Establece el valor de una celda.
     * 
     * @param bookName
     *            - Nombre del libro.
     * @param sheetName
     *            - Nombre de la hoja.
     * @param colRow
     *            - ColumnaFila (A23).
     * @param rawValue
     *            - Valor a establecer.
     */
    public void setCellValue(String bookName, String sheetName, String colRow, String rawValue);

    /**
     * Cambia el formato de una celda a String.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatString(String bookName, String sheet, String colRow)
            throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Numero.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param decimal
     *            : es la cantidad de decimales.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatNumber(String bookName, String sheet, String colRow, int decimal)
            throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Moneda.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param decimal
     *            : es la cantidad de decimales.
     * @param symbol
     *            : es el simbolo de la moneda que selecciono el usuario.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatMoney(String bookName, String sheet, String colRow, String symbol)
            throws InvalidCellIdentifierException;

    /**
     * Cambia el formato de una celda a Fecha.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param colRow
     *            : columna y fila de la celda que se quiere modificar.
     * @param dateOrder
     *            : es orden del a�o, mes y dia.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public void setCellFormatDate(String bookName, String sheet, String colRow, String dateOrder)
            throws InvalidCellIdentifierException;

    /**
     * Imprime la tabla por pantalla.
     */
    public void printTable();

    /**
     * Deshace la ultima modificacion que se hizo.
     */
    public void undo();

    /**
     * Rehace la ultima modificacion que se deshizo.
     */
    public void redo();

    /**
     * Elimina una hoja de un libro.
     * 
     * @param bookName
     *            : nombre del libro.
     * @param sheetName
     *            : nombre de la hoja.
     */
    void removeWorkSheetNamed(String bookName, String sheetName);

    /**
     * Agrega un valor a una celda en la hoja y el libro actual.
     * 
     * @param colRow
     *            : posicion de la celda.
     * @param rawValue
     *            : valor de la celda que se quiere setear.
     */
    void setCellValueInTheCurrentSheetAndBook(String colRow, String rawValue);

    /**
     * Abrir una hoja de un libro.
     * 
     * @param bookName
     *            : nombre del libro.
     * @param sheetName
     *            : nombre de la hoja.
     */
    void openSheet(String bookName, String sheetName);

    /**
     * Persiste la hoja actual en un CSV.
     * 
     * @param fileName
     *            : nombre del archivo.
     */
    void persistCurrentSheet(String fileName);

    /**
     * Exporta un libro a un archivo.
     * 
     * @param book
     *            : es el nombre del libro que se quiere exportar.
     * @param fileName
     *            : direccion del archivo donde se debe crear.
     */
    void exportBook(String book, String fileName);

    /**
     * Importa un libro de un archivo.
     * 
     * @param book
     *            : es el nombre del libro que se quiere Importa.
     * @param fileName
     *            : direccion del archivo que se quiere importar.
     */
    void importBook(String book, String fileName);
    
    /**
     * Importa una hoja de un archivo.
     * 
     * @param book
     *            : es el nombre del libro que se quiere Importa.
     * @param sheet
     *            : es el nombre de la hoja que se quiere Importa.
     * @param fileName
     *            : direccion del archivo que se quiere Importa.
     */
    void importCVS(String book, String sheet, String fileName);

}
