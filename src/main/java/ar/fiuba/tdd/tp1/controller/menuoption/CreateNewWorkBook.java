package ar.fiuba.tdd.tp1.controller.menuoption;

import ar.fiuba.tdd.tp1.controller.ControllerWindow;
import ar.fiuba.tdd.tp1.controller.MenuOption;

/**
 * Esta clase se encarga de crear un libro en el controller.
 * 
 * @author Galli
 *
 */
public class CreateNewWorkBook extends MenuOption {

    public CreateNewWorkBook() {
        numParameter = 2;
    }

    @Override
    protected void executeMethod(String[] parameter, ControllerWindow controller) {
        controller.createNewWorkBookNamed(parameter[1]);
    }

}
