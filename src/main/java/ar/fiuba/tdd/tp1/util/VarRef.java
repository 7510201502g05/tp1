package ar.fiuba.tdd.tp1.util;

import java.util.regex.Pattern;

/**
 * Representa una clase utilitaria para el manejo de referencias a variables como por ejemplo,
 * validacion de nombres de variables, construccion de referencias a variables, etc. <br/>
 * Ejemplo de uso:
 * 
 * <code>VarRef.get().relativeMatcher().matches("C23")</code>
 * 
 * @author martin
 *
 */
public class VarRef {
    private static VarRef instance = new VarRef();
    public static final String PAGE_SEP = "!";
    public static final String RANGE_SEP = ":";

    private String relativeRegex = "[A-Z]+\\d+";
    private String absoluteRegex = "\\w+" + PAGE_SEP + relativeRegex;
    private Pattern relativePattern;
    private Pattern absolutePattern;

    private String relativeRangeRegex = relativeRegex + RANGE_SEP + relativeRegex;
    private String absoluteRangeRegex = absoluteRegex + RANGE_SEP + absoluteRegex;
    private Pattern relativeRangePattern;
    private Pattern absoluteRangePattern;

    private String miscRegex = "(\\s*[a-zA-Z��_0-9]+\\s*)+";
//    private String miscRegex = "(\\s*\\w+\\s*)+";
//    private String miscRegex = "(\\w+\\s*)+";
    private Pattern miscPattern;
    
    private String doubleRegex = "\\d*\\.?\\d+";
    private Pattern doublePattern;

    private String colRowSplitRegex = "(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)";

    public static VarRef get() {
        return instance;
    }

    protected VarRef() {
        relativePattern = Pattern.compile(relativeRegex);
        absolutePattern = Pattern.compile(absoluteRegex);
        relativeRangePattern = Pattern.compile(relativeRangeRegex);
        absoluteRangePattern = Pattern.compile(absoluteRangeRegex);
        miscPattern = Pattern.compile(miscRegex);
        doublePattern = Pattern.compile(doubleRegex);
    }

    public String miscRegex() {
        return miscRegex;
    }

    public String colRowSplitRegex() {
        return colRowSplitRegex;
    }

    public String relativeRegex() {
        return relativeRegex;
    }

    public String absoluteRegex() {
        return absoluteRegex;
    }

    public String relativeRangeRegex() {
        return relativeRangeRegex;
    }

    public String absoluteRangeRegex() {
        return absoluteRangeRegex;
    }
    
    public String doubleRegex() {
        return doubleRegex;
    }

    @FunctionalInterface
    public static interface VarMatcher {
        public boolean matches(String expression);
    }

    public VarMatcher relativeMatcher() {
        return expr -> relativePattern.matcher(expr).matches();
    }

    public VarMatcher absoluteMatcher() {
        return expr -> absolutePattern.matcher(expr).matches();
    }

    public VarMatcher relativeRangeMatcher() {
        return expr -> relativeRangePattern.matcher(expr).matches();
    }

    public VarMatcher absoluteRangeMatcher() {
        return expr -> absoluteRangePattern.matcher(expr).matches();
    }

    public VarMatcher miscMatcher() {
        return expr -> {
            boolean isMisc = false;
            isMisc = isMisc || miscPattern.matcher(expr).matches();
            isMisc = isMisc && !get().absoluteMatcher().matches(expr);
            isMisc = isMisc && !get().relativeMatcher().matches(expr);
            return isMisc;
        };
    }
    
    public VarMatcher doubleMatcher() {
        return expr -> doublePattern.matcher(expr).matches();
    }

    @FunctionalInterface
    public static interface StringPairBuilder {
        public String build(String pointer, String relVal);
    }

    public StringPairBuilder rangeBuilder() {
        return (topLeft, bottomRight) -> {
            return topLeft + RANGE_SEP + bottomRight;
        };
    }

    /**
     * Constructor de variables absolutas.
     * 
     * @return Constructor de variables absolutas.
     */
    public StringPairBuilder absVarReferenceBuilder() {
        return (pointer, relVar) -> {
            if (VarRef.get().relativeMatcher().matches(relVar) && !pointer.isEmpty()) {
                return pointer + PAGE_SEP + relVar;
            }

            return relVar;
        };
    }

    /**
     * Constructor de variables relativas.
     * 
     * @return Constructor de variables relativas.
     */
    public StringPairBuilder relVarReferenceBuilder() {
        return (col, row) -> {
            if (col == null || row == null) {
                return "";
            }
            return col + row;
        };
    }
}
