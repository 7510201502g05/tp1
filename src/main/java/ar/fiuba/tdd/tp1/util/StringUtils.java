package ar.fiuba.tdd.tp1.util;

import java.util.List;

/**
 * Representa utilidades de Strings.
 * 
 * @author martin
 *
 */
public class StringUtils {
    /**
     * Retorna true si el string NO esta vacio.
     * 
     * @param str
     *            - String a verificar.
     * @return true si el string NO esta vacio.
     */
    public static boolean notEmpty(String str) {
        return str.isEmpty() == false;
    }

    /**
     * Retorna true si el string es nulo o vacio.
     * 
     * @param str
     *            - String a verificar.
     * @return true si el string es nulo o vacio.
     */
    public static boolean nullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    /**
     * une todos los elementos de una lista como un unico String.
     * 
     * @param list
     *            - Lista a unificar.
     * @return todos los elementos de una lista como un unico String.
     */
    public static String joinAllAsString(List<?> list) {
        final StringBuilder stringBuilder = new StringBuilder();
        list.forEach(elem -> stringBuilder.append(elem.toString()));
        return stringBuilder.toString();
    }
}
