package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

/**
 * Es la interfaz que deben implementar los comandos.
 * 
 * @author Galli
 *
 */
public interface Command {

    /**
     * Ejecuta una accion.
     * 
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar era invalida.
     */
    public void execute() throws InvalidCellIdentifierException;

    /**
     * Deshace la accion que se ejecuto, en caso de no haberse ejecutado el comando no se hara nada.
     */
    public void undo();

}
