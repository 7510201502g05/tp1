package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.controller.SetterCurrentSheetAndBook;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

import java.util.Optional;

/**
 * Esta clase se encarga de modificar el nombre de la hoja y el libro del setter.
 * 
 * @author Galli
 *
 */
public class ModifyCurrentSheetAndBook implements Command {

    private String newBook;
    private String newSheet;
    private String oldBook;
    private String oldSheet;
    private SetterCurrentSheetAndBook setter;

    public ModifyCurrentSheetAndBook(String newBook, String newSheet, String oldBook,
            String oldSheet, SetterCurrentSheetAndBook setter) {
        Optional.ofNullable(newBook).ofNullable(newSheet).ofNullable(oldBook).ofNullable(oldSheet)
                .ofNullable(setter);
        this.newBook = newBook;
        this.newSheet = newSheet;
        this.oldBook = oldBook;
        this.oldSheet = oldSheet;
        this.setter = setter;
    }

    @Override
    public void execute() throws InvalidCellIdentifierException {
        setter.setCurrentSheetAndBook(newSheet, newBook);
    }

    @Override
    public void undo() {
        setter.setCurrentSheetAndBook(oldSheet, oldBook);
    }

}
