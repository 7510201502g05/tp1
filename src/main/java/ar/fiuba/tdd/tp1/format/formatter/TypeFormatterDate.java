package ar.fiuba.tdd.tp1.format.formatter;

/**
 * Esta interfaz la deben implementar las clases que devuelven fechas en Strings con un cierto
 * orden.
 * 
 * @author Galli
 *
 */
public interface TypeFormatterDate {

    /**
     * Devuelve un string con los datos ordenados del a�o, mes y dia que se pasan por parametro.
     * 
     * @param year
     *            : a�o.
     * @param month
     *            : mes.
     * @param day
     *            : dia.
     * @return un string con los datos pasados por parametro.
     */
    public String format();

    /**
     * Devuelve el tipo de formato que se usa.
     * 
     * @return el formato que tiene.
     */
    public String getInformation();
    

}
