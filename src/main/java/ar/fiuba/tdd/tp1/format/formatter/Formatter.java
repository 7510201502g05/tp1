package ar.fiuba.tdd.tp1.format.formatter;

import java.util.Map;

/**
 * Esta clase se encarga de darle un formato a un string.
 * 
 * @author Galli
 *
 */
public interface Formatter {

    /**
     * Devuelve un string con el valor formateado.
     * 
     * @param value
     *            : es el valor que se quiere formatear.
     * @return un string con el valor formateado.
     */
    public String format(String value);

    /**
     * Devuelve un mapa con la informacion del formato.
     * 
     * @return mapa con la informacion.
     */
    public Map<String, String> getInformation();

}
