package ar.fiuba.tdd.tp1.format.formatter;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Optional;

/**
 * Es el formato de numero con la cantidad de decimales que el usuario desee.
 * 
 * @author Galli
 *
 */
public class FNumber implements Formatter {

    private String decimalFormat;
    private int decimal;
    private Formatter otherFormatter;

    public FNumber(int numberDecimal, Formatter otherFormatter) {
        setData(numberDecimal, otherFormatter);
    }

    public FNumber(String value, Formatter otherFormatter2) {
        this.setData(Integer.parseInt(value), otherFormatter2);
    }

    /**
     * Setea los valores.
     * 
     * @param numberDecimal
     *            : cantidad de decimales.
     * @param otherFormatter
     *            : otro formato.
     */
    private void setData(int numberDecimal, Formatter otherFormatter) {
        Optional.of(numberDecimal).of(otherFormatter);
        if (numberDecimal < 0) {
            throw new InvalidFormatException(
                    "La cantidad de decimales tiene que ser mayor o igual que cero.");
        }
        decimal = numberDecimal;
        this.decimalFormat(numberDecimal);
        this.otherFormatter = otherFormatter;
    }

    /**
     * Devuelve el formato decimal que se desea apartir de la cantidad de decimales que se quiere
     * que tenga un numero.
     * 
     * @param numberDecimal
     *            : es la cantidad de decimales que se quiere obtener.
     */
    private void decimalFormat(int numberDecimal) {
        decimalFormat = "#";
        if (numberDecimal > 0) {
            decimalFormat += ".";
        }
        for (int i = 0; i < numberDecimal; i++) {
            decimalFormat += "0";
        }
    }

    @Override
    public String format(String value) {
        if (value.equals("")) {
            return "";
        }
        try {
            Double doubleVal = Double.valueOf(value);
            DecimalFormat df = new DecimalFormat(decimalFormat);
            df.setRoundingMode(RoundingMode.HALF_UP);
            String newValue = (df.format(doubleVal.doubleValue())).replaceAll(",", ".");
            return otherFormatter.format(newValue);
        } catch (NumberFormatException e) {
            return "Error:BAD_NUMBER";
//            throw new InvalidFormatException("El valor: " + value
//                    + " no se le puede poner formato numero");
        }
    }

    @Override
    public Map<String, String> getInformation() {
        CreateInformation information = new CreateInformation(otherFormatter.getInformation());
        if (information.getInformation("Type") != "Moneda") {
            information.addInformation("Type", "Number");
        }
        information.addInformation("Decimal", "" + decimal);
        return information.getInformation();
    }

}
