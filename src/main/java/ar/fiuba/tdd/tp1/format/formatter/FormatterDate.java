package ar.fiuba.tdd.tp1.format.formatter;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * Es el formato de fecha.
 * 
 * @author Galli
 *
 */
public class FormatterDate implements Formatter {

    private String formatter;

    public FormatterDate(String formatter) {
        Optional.of(formatter);
        FDate date = new FDate();
        if (!date.isAValidFormat(formatter)) {
            throw new InvalidFormatException("El formato pedido: " + formatter
                    + " no es un formato valido.");
        }
        this.formatter = formatter.toLowerCase(Locale.ROOT).replaceAll("m", "M");
    }
    
    public FormatterDate() {
        formatter = "";
    }

    @Override
    public String format(String value) {
        NumberFormat format = NumberFormat.getInstance(Locale.US);
        if (value.equals("")) {
            return value;
        }
        try {
            Number number = format.parse(value);
            long numberLong = number.longValue();
            FDate date = new FDate(numberLong);
            return date.getFormattedDate(formatter);
        } catch (Exception e) {
            return "Error:BAD_DATE";
//            throw new InvalidFormatException("El valor: " + value
//                    + " no se le puede poner formato numero");
        }
    }

    @Override
    public Map<String, String> getInformation() {
        CreateInformation information = new CreateInformation();
        information.addInformation("Type", "Date");
        information.addInformation("Format", formatter.toUpperCase(Locale.ROOT));
        return information.getInformation();
    }

}
