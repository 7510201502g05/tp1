package ar.fiuba.tdd.tp1.model.sheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CellImpl;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cellcontainer.CellContainer;
import ar.fiuba.tdd.tp1.model.cellcontainer.CrpCellContainer;

import java.util.Map;

/**
 * Implementacion de una hoja con celdas identificadas con el par (fila, columna).
 *
 */
public class CrpSheet<DataTypeT> implements Sheet<CrPair, DataTypeT> {

    String sheetName;
    CellContainer<CrPair, DataTypeT> cc;

    /**
     * Construye una hoja con un contenedor y nombre y la deja en un estado operacionalmente valido.
     * 
     * @param sheetName
     *            : El nombre de la hoja.
     * @param cc
     *            : El contenedor de celdas de la hoja.
     */
    public CrpSheet(String sheetName, CrpCellContainer<DataTypeT> cc) {
        this.sheetName = sheetName;
        this.cc = cc;
    }

    @Override
    public String getSheetName() {
        return this.sheetName;
    }

    @Override
    public Sheet<CrPair, DataTypeT> putCell(String column, int row, DataTypeT rawValue)
            throws InvalidCellIdentifierException {
        try {
            CellImpl<CrPair, DataTypeT> newCell = new CellImpl<CrPair, DataTypeT>(new CrPair(row,
                    column), rawValue);
            this.cc.putCell(newCell);
            return this;
        } catch (Exception e) {
            throw new InvalidCellIdentifierException("Identificador de celda " + column + row
                    + " invalido!");
        }
    }

    @Override
    public Sheet<CrPair, DataTypeT> renameSheet(String newName) {
        this.sheetName = newName;
        return this;
    }

    @Override
    public Cell<CrPair, DataTypeT> getCell(String column, int row)
            throws InvalidCellIdentifierException {
        CrPair cellId = new CrPair(row, column);
        return this.cc.getCell(cellId);
    }

    @Override
    public Sheet<CrPair, DataTypeT> setCellContainer(CellContainer<CrPair, DataTypeT> cellCont) {
        this.cc = cellCont;
        return this;
    }

    @Override
    public Sheet<CrPair, DataTypeT> clearCell(String column, int row) throws InvalidCellIdentifierException {
        cc.removeCell(new CrPair(row, column));
        return this;
    }

    @Override
    public DataTypeT getCellRawValue(String column, int row) throws InvalidCellIdentifierException {
        try {
            return this.getCell(column, row).getRawValue();
        } catch (InvalidCellIdentifierException e) {
            throw e;
        }
    }

    @Override
    public Map<CrPair, Cell<CrPair, DataTypeT>> getCellsInSheet() {
        return this.cc.getCells();
    }

}
