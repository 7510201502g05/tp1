package ar.fiuba.tdd.tp1.model.cell;

public interface Cell<IdType, DataType> {

    /**
     * Setea el valor crudo de la celda.
     * 
     * @param value
     *            : valor a asignarle a la celda
     *
     * @return : devuelve la celda con su valor ya seteado
     */
    public Cell<IdType, DataType> setRawValue(DataType value);

    /**
     * Devuelve el valor crudo de la celda.
     * 
     * @return : valor contenido en celda.
     */
    public DataType getRawValue();

    /**
     * Devuelve la identificacion de la celda.
     * 
     * @return : tipo de identificacion de celda.
     */
    public IdType getId();
}
