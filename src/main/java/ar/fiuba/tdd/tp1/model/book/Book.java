package ar.fiuba.tdd.tp1.model.book;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.sheet.Sheet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface Book<CellIdentityTypeT, DataTypeT> {

    /**
     * Crea una hoja con un nombre dado.
     * 
     * @param name
     *            : nombre de la hoja a crear.
     * @return : devuelve el libro con la hoja creada.
     */
    public Book<CellIdentityTypeT, DataTypeT> createSheet(String name);

    /**
     * Renombra una hoja perteneciente al libro con un nuevo nombre.
     * 
     * @param oldName
     *            : el nombre a buscar de la hoja a renombrar.
     * @param newName
     *            : el nuevo nombre para la hoja buscada.
     * @return : Devuelve el libro con la hoja renombrada.
     */
    public Book<CellIdentityTypeT, DataTypeT> renameSheet(String oldName, String newName);

    /**
     * Remueve una hoja especificada del libro.
     * 
     * @param name
     *            : nombre de la hoja a borrar.
     * @return : devuelve el libro con su hoja borrada.
     */
    public Book<CellIdentityTypeT, DataTypeT> removeSheet(String name);

    /**
     * Devuelve una hoja solicitada.
     * 
     * @param name
     *            : Nombre de la hoja a buscar.
     * @return : devuelve la hoja buscada del libro.
     */
    public Sheet<CellIdentityTypeT, DataTypeT> getSheet(String name);

    /**
     * Determina si una hoja determinada existe en el libro.
     * 
     * @param name
     *            : Nombre d ela hoja a buscar
     * @return : Devuelve verdadero, en caso de que la hoja exista, caso contrario devuelve falso.
     */
    public boolean existsSheet(String name);

    /**
     * Coloca una celda en una hoja determinada del libro.
     * 
     * @param sheet
     *            : Nombre de la hoja donde se colocara la celda.
     * @param col
     *            : Columna de la hoja donde se colocara la celda (debe ser una letra).
     * @param row
     *            : Fila de la hoja donde se colocara la celda (debe ser un numero natural).
     * @param rawValue
     *            : valor de la celda a asignar.
     * @return : Devuelve el libro con la celda asignada en la hoja correspondiente.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion si el el par (fila, columna) es invalido.
     */
    public Book<CellIdentityTypeT, DataTypeT> putCell(String sheet, String col, int row, DataTypeT rawValue)
            throws InvalidCellIdentifierException;

    /**
     * Borra el valor de una celda en una hoja determinada del libro.
     * 
     * @param sheet
     *            : Nombre de la hoja.
     * @param col
     *            : Columna de la hoja donde se colocara la celda (debe ser una letra).
     * @param row
     *            : Fila de la hoja donde se colocara la celda (debe ser un numero natural).
     * @return : Devuelve el libro con la celda determinada en su valor en blanco.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion si el el par (fila, columna) es invalido.
     */
    public Book<CellIdentityTypeT, DataTypeT> clearCell(String sheet, String col, int row)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor de la celda, especificando su hoja, fila y columna.
     * 
     * @param sheet
     *            : Nombre de la hoja.
     * @param col
     *            : Columna de la hoja donde se colocara la celda (debe ser una letra).
     * @param row
     *            : Fila de la hoja donde se colocara la celda (debe ser un numero natural).
     * @return : Devuelve el valor de la celda solicitada.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion si el el par (fila, columna) es invalido.
     */
    public DataTypeT getCellRawValue(String sheet, String col, int row)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor de la celda, especificando un string que contiene el Id.
     * 
     * @param cellId
     *            : String con id de celda.
     * @return : Devuelve el valor de la celda solicitada.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion si el el par (fila, columna) es invalido.
     */
    public DataTypeT getCellRawValue(String cellId) throws InvalidCellIdentifierException;

    /**
     * Devuelve una lista con los nombres de las hojas.
     * 
     * @return lista de nombres.
     */
    public List<String> getNamesSheet();

    /**
     * Se inserta la hoja en el libro.
     * 
     * @param sheet
     *            : hoja que se quiere colocar en el book.
     * @return el libro.
     */
    public Book<CellIdentityTypeT, DataTypeT> putSheet(Sheet<CellIdentityTypeT, DataTypeT> sheet);

    /**
     * Devuelve las celdas que guardaba cada hoja del libro.
     * 
     * @return un mapa.
     */
    public HashMap<String, Map<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>>> getCellsInBook();
    
}
