package ar.fiuba.tdd.tp1.model.sheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.cellcontainer.CellContainer;

import java.util.Map;

public interface Sheet<CellIdentityTypeT, DataTypeT> {

    /**
     * 
     * @return : el nombre de la hoja.
     */
    public String getSheetName();

    /**
     * Inserta un valor en la celda perteneciente a la hoja.
     * 
     * @param column
     *            : una columna representada por un caracter de letra.
     * @param row
     *            : una fila representada por un numero entero.
     * @param rawValue
     *            : el valor a asignar a la celda dentro de la hoja.
     * @return : devuelve la hoja en su estado luego de aplicar el metodo.
     * @throws InvalidCellIdentifierException
     *             : devuelve esta excepcion en caso de que el par (fila, columna) sea invalido.
     */
    public Sheet<CellIdentityTypeT, DataTypeT> putCell(String column, int row, DataTypeT rawValue)
            throws InvalidCellIdentifierException;

    /**
     * Cambia el nombre de la hoja.
     * 
     * @param newName
     *            : nuevo nombre a asignar.
     * @return : La hoja con su nuevo nombre.
     */
    public Sheet<CellIdentityTypeT, DataTypeT> renameSheet(String newName);

    /**
     * Devuelve la celda solicitada.
     * 
     * @param column
     *            : una columna representada por un caracter de letra.
     * @param row
     *            : una fila representada por un numero entero.
     * @return : Devuelve la celda solicitada perteneciente a la hoja.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion en caso de que el par (fila, columna) sea invalido.
     */
    public Cell<CrPair, DataTypeT> getCell(String column, int row)
            throws InvalidCellIdentifierException;

    /**
     * Asigna el contenedor de celdas a la hoja.
     * 
     * @param cellCont
     *            : un contenedor de celdas.
     * @return : la hoja con su contenedor asignado.
     */
    public Sheet<CellIdentityTypeT, DataTypeT> setCellContainer(
            CellContainer<CellIdentityTypeT, DataTypeT> cellCont);

    /**
     * Borra el valor de la celda, espeficando cual.
     * 
     * @param column
     *            : una columna representada por un caracter de letra.
     * @param row
     *            : una fila representada por un numero entero.
     * @return : Devuelve la hoja con el valor de la celda solicitada borrada.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion en caso de que el par (fila, columna) sea invalido.
     */
    public Sheet<CellIdentityTypeT, DataTypeT> clearCell(String column, int row)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor de la celda solicitada.
     * 
     * @param column
     *            : una columna representada por un caracter de letra.
     * @param row
     *            : una fila representada por un numero entero.
     * @return : Devuelve el valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : Devuelve esta excepcion en caso de que el par (fila, columna) sea invalido.
     */
    public DataTypeT getCellRawValue(String column, int row) throws InvalidCellIdentifierException;

    
    /**
     * Devuelve un mapa con las celdas que guardaba la hoja.
     * 
     * @return un mapa.
     */
    public Map<CellIdentityTypeT, Cell<CellIdentityTypeT, DataTypeT>> getCellsInSheet();
}
