package ar.fiuba.tdd.tp1.model.cell;

import java.util.Objects;


public class CellImpl<IdTypeT,DataTypeT> implements Cell<IdTypeT,DataTypeT> {
    private IdTypeT id;
    private DataTypeT value;

    /**
     * Construye una nueva celda y la deja en un estado valido, con un id, y su valor vacio.
     * 
     * @param id : tipo de Id que dara su identificacion a la celda.
     */
    public CellImpl(IdTypeT id) {
        super();
        this.id = id;
    }

    /**
     * Construye una nueva celda y la deja en un estado valido, con un id y valor dado.
     * 
     * @param id : tipo de Id que dara su identificacion a la celda.
     * @param value : valor que se contendra dentro de la celda.
     */
    public CellImpl(IdTypeT id, DataTypeT value) {
        super();
        this.id = id;
        this.value = value;
    }

    @Override
    public Cell<IdTypeT,DataTypeT> setRawValue(DataTypeT value) {
        this.value = value;
        return this;
    }

    @Override
    public DataTypeT getRawValue() {
        return value;
    }

    @Override
    public IdTypeT getId() {
        return this.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object object) {
        if (object instanceof CellImpl) {
            CellImpl<IdTypeT,DataTypeT> that = (CellImpl<IdTypeT,DataTypeT>) object;
            return Objects.equals(this.id, that.id) && Objects.equals(this.value, that.value);
        }
        return false;
    }
}
