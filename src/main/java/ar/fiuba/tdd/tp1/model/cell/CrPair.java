package ar.fiuba.tdd.tp1.model.cell;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

import java.util.Objects;

/**
 * Representacion de un identificador de celda, con la forma (fila, columna).
 */
public class CrPair {
    private int row;
    private String column;

    /**
     * Contruye un identificador, dejandolo en un estado valido.
     * 
     * @param row
     *            : fila del identificador.
     * @param column
     *            : columna del identificador.
     */
    public CrPair(int row, String column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Devuelve el numero de fila del identificador.
     * 
     * @return : la fila del identificador (valor numerico)
     */
    public int getRow() {
        return this.row;
    }

    /**
     * Devuelve la columna del identificador.
     * 
     * @return : la columna del identificador (un caracter de letra)
     */
    public String getColumn() {
        return this.column;
    }

    @SuppressWarnings("CPD-START")
    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof CrPair) {
            CrPair that = (CrPair) object;
            return Objects.equals(this.row, that.row) && Objects.equals(this.column, that.column);
        }
        return false;
    }

    @SuppressWarnings("CPD-END")
    @Override
    public String toString() {
        return this.column + String.valueOf(this.row);
    }

    public static CrPair create(String cellRef) throws InvalidCellIdentifierException {
        String[] columnRow = cellRef.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
        try {
            return new CrPair(Integer.parseInt(columnRow[1]), columnRow[0]);
        //        return new CrPair(Integer.valueOf(columnRow[1]).intValue(), columnRow[0]);
        } catch (Exception e) {
            throw new InvalidCellIdentifierException();
        }
    }
}
