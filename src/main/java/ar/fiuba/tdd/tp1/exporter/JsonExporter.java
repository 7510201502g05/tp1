package ar.fiuba.tdd.tp1.exporter;

import com.google.gson.Gson;

import ar.fiuba.tdd.tp1.exceptions.fileio.FileCreationException;
import ar.fiuba.tdd.tp1.exceptions.fileio.InvalidDirectoryNameException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;
import ar.fiuba.tdd.tp1.model.modelcontainer.CellModel;
import ar.fiuba.tdd.tp1.model.modelcontainer.FormatterModel;
import ar.fiuba.tdd.tp1.model.modelcontainer.SpreadSheetModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonExporter implements BookExporter {

    @Override
    public void export(HashMap<String, Map<CrPair, Cell<CrPair, String>>> cellValueMap,
            HashMap<String, Map<CrPair, Map<String, String>>> cellFormatsMap, String filePath,
            String fileName) {

        SpreadSheetModel ssModel = new SpreadSheetModel();
        List<CellModel> cellList = new ArrayList<CellModel>();
        
        String[] bookName = fileName.split("\\.");
        ssModel.setName(bookName[0]);
        buildSpreadSheetModel(cellValueMap, cellFormatsMap, ssModel, cellList);

        Gson gson = new Gson();
        String jsonFileContent = gson.toJson(ssModel);

        
        checkFilePath(new File(filePath));
        File file = new File(filePath + fileName);
//        checkFilePath(file);

        createFileToExport(file);
        OutputStream output = null;
        output = exportToFile(filePath, fileName, output, jsonFileContent);

        try {
            if (output != null) {
                output.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkFilePath(File file) {
        if (!file.exists()) {
            if (!file.getParentFile().mkdirs()) {
                throw new InvalidDirectoryNameException(
                        "La ruta de directorio especificada no es valida.");
            }
        }
    }

    private OutputStream exportToFile(String filePath, String fileName, OutputStream output,
            String jsonFileContent) {
        try {
            output = new FileOutputStream(filePath + fileName);

            byte[] contentInBytes = null;
            try {
                contentInBytes = jsonFileContent.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                if (contentInBytes != null) {
                    output.write(contentInBytes);
                }

            } finally {
                output.flush();
                output.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

    private void createFileToExport(File file) {
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new FileCreationException("No se pudo crear el archivo.");
        }
    }

    private void buildSpreadSheetModel(
            HashMap<String, Map<CrPair, Cell<CrPair, String>>> cellValueMap,
            HashMap<String, Map<CrPair, Map<String, String>>> cellFormatsMap,
            SpreadSheetModel ssModel, List<CellModel> cellList) {
        ssModel.setVersion("1.0");

        cellValueMap.forEach((sheet, contenedor) -> {
                contenedor.forEach((id, cell) -> {
                        CellModel cellModel = new CellModel();
                        cellModel.setCellId(cell.getId().toString());
                        cellModel.setSheetName(sheet);
                        cellModel.setValue(cell.getRawValue());
                        cellModel.setType(cellFormatsMap.get(sheet).get(id).get("Type"));
                        FormatterModel cellFormatter = this.getFormatFromMap(cellFormatsMap, sheet, id);
                        cellModel.setFormatter(cellFormatter.getFormatter());
                        cellList.add(cellModel);
                    }
                );
            }
        );

        ssModel.setCells(cellList);
    }

    private FormatterModel getFormatFromMap(
            HashMap<String, Map<CrPair, Map<String, String>>> cellFormatsMap, String sheetName,
            CrPair id) {
        FormatterModel cellFormatter = new FormatterModel();
        HashMap<String, String> formatterMap = new HashMap<String, String>();

        String type = cellFormatsMap.get(sheetName).get(id).get("Type");
        cellFormatsMap.get(sheetName).get(id).remove("Type");

        cellFormatsMap.get(sheetName).get(id).forEach((parameter, value) -> {
                formatterMap.put(type + "." + parameter, value);
            }
        );

        cellFormatter.setFormatter(formatterMap);
        return cellFormatter;
    }

}
