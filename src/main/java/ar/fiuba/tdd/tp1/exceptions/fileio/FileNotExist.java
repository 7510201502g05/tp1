package ar.fiuba.tdd.tp1.exceptions.fileio;

public class FileNotExist extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public FileNotExist(String msg) {
        super(msg);
    }
}
