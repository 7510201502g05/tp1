package ar.fiuba.tdd.tp1.exceptions.model;

public class SheetNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SheetNotFoundException(String msg) {
        super(msg);
    }

}
