package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Esta es la interfaz que el usuario conocera.
 *
 * @author Galli
 *
 */
public interface SpreadSheet<DataTypeT> extends SpreadSheetUndoRedo<SpreadSheet<DataTypeT>> {

    /**
     * Cambia el valor de una celda.
     * 
     * @param sheet
     *            : pagina que se quiere modificar.
     * @param col
     *            : columna que se quiere modificar.
     * @param row
     *            : fila que se quiere modificar.
     * @param rawValue
     *            : nuevo valor de la celda.
     * @throws InvalidCellIdentifierException
     *             : la celda que se quiso modificar es invalida.
     */
    public SpreadSheet<DataTypeT> putCell(String sheet, String col, Integer row, DataTypeT rawValue)
            throws InvalidCellIdentifierException;

    /**
     * Obtiene el valor crudo de la celda que se busca.
     * 
     * @param sheet
     *            : pagina que se busca.
     * @param col
     *            : columna que se busca.
     * @param row
     *            : fila que se busca.
     * @return el valor crudo, sin procesar, que guarda la celda.
     * @throws InvalidCellIdentifierException
     *             : los parametros de las celdas son invalidos.
     */
    public DataTypeT getCellRawValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException;

    /**
     * Crea una hoja con el nombre que se pasa por parametro.
     * 
     * @param name
     *            : nombre de la hoja.
     */
    public SpreadSheet<DataTypeT> createSheet(String name);

    /**
     * Borra la hoja del libro que tiene el nombre que se pasa por parametro.
     * 
     * @param name
     *            : nombre de la hoja a eliminar.
     */
    public SpreadSheet<DataTypeT> removeSheet(String name);

    /**
     * Cambia el nombre de la hoja oldName por newName.
     * 
     * @param oldName
     *            : nombre de la hoja la cual se quiere modificar.
     * @param newName
     *            : nuevo nombre para la hoja.
     */
    public SpreadSheet<DataTypeT> renameSheet(String oldName, String newName);

    /**
     * Revisa si existe la hoja con el nombre pasado por parametro.
     * 
     * @param name
     *            : nombre de la hoja que se busca.
     * @return true si existe y false en caso contrario.
     */
    public boolean existsSheet(String name);

    /**
     * Devuelve los nombres de las hojas.
     * 
     * @return lista con los nombres de las hojas.
     */
    public List<String> getNamesSheet();

    /**
     * Devuelve un mapa con los valores de las celdas de cada pagina.
     * 
     * @return un mapa.
     */
    public HashMap<String, Map<CrPair, Cell<CrPair, DataTypeT>>> getCellsInBook();

}
