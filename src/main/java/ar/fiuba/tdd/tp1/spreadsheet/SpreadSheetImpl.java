package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.command.CommandExecutor;
import ar.fiuba.tdd.tp1.command.CommandExecutorImpl;
import ar.fiuba.tdd.tp1.command.SetCellValueCommand;
import ar.fiuba.tdd.tp1.command.SheetCreateCommand;
import ar.fiuba.tdd.tp1.command.SheetDeleteCommand;
import ar.fiuba.tdd.tp1.command.SheetRenameCommand;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.book.CrpBook;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Se encarga de manejar los cambios en el libro de calculos.
 *
 * @author Galli
 */
public abstract class SpreadSheetImpl<DataTypeT> implements CellVarIdAccessor<DataTypeT>,
        SpreadSheet<DataTypeT> {

    protected Book<CrPair, DataTypeT> book;
    private CommandExecutor commandExecutor;

    public SpreadSheetImpl() {
        book = new CrpBook<DataTypeT>();
        commandExecutor = new CommandExecutorImpl();
    }

    @Override
    public SpreadSheetImpl<DataTypeT> putCell(String sheet, String col, Integer row,
            DataTypeT rawValue) throws InvalidCellIdentifierException {
        commandExecutor.runCommand(new SetCellValueCommand<CrPair, DataTypeT>(book, sheet, col,
                row, rawValue));
        return this;
    }

    @Override
    public DataTypeT getCellRawValue(String cellId) throws InvalidCellIdentifierException {
        return this.returnRawValue(book.getCellRawValue(cellId));
    }

    @Override
    public DataTypeT getCellRawValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException {
        return this.returnRawValue(book.getCellRawValue(sheet, col, row));
    }

    @Override
    public SpreadSheetImpl<DataTypeT> createSheet(String name) {
        commandExecutor.runCommand(new SheetCreateCommand<CrPair, DataTypeT>(name, book));
        return this;
    }

    @Override
    public SpreadSheetImpl<DataTypeT> removeSheet(String name) {
        commandExecutor.runCommand(new SheetDeleteCommand<CrPair, DataTypeT>(name, book));
        return this;
    }

    @Override
    public SpreadSheetImpl<DataTypeT> renameSheet(String oldName, String newName) {
        commandExecutor
                .runCommand(new SheetRenameCommand<CrPair, DataTypeT>(oldName, newName, book));
        return this;
    }

    @Override
    public boolean existsSheet(String name) {
        return book.existsSheet(name);
    }

    @Override
    public SpreadSheet<DataTypeT> undo() {
        commandExecutor.undo();
        return this;
    }

    @Override
    public SpreadSheet<DataTypeT> redo() {
        commandExecutor.redo();
        return this;
    }

    @Override
    public List<String> getNamesSheet() {
        return book.getNamesSheet();
    }
    
    @Override
    public HashMap<String, Map<CrPair, Cell<CrPair, DataTypeT>>> getCellsInBook() {
        return book.getCellsInBook();
    }

    /**
     * Devuelve un DataTypeT, si el rawValue es null devuelve el DataTypeT que corresponda, en caso
     * contrario devuelve DataTypeT.
     * 
     * @param rawValue
     *            : valor que se quiere retornar.
     * @return DataTypeT que corresponda.
     */
    protected abstract DataTypeT returnRawValue(DataTypeT rawValue);

}
