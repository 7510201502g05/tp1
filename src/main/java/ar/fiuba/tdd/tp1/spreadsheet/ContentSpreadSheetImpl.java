package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.evaluator.CellInterpreter;
import ar.fiuba.tdd.tp1.evaluator.CellInterpreterImpl;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;

import java.util.Optional;

/**
 * Es el SpreadSheet que guarda Strings en su contenido que pueden ser formulas y tambien las puede
 * evaluar.
 * 
 * @author Galli
 *
 */
public class ContentSpreadSheetImpl extends SpreadSheetImpl<String> implements ContentSpreadSheet {

    @Override
    protected String returnRawValue(String rawValue) {
        return Optional.ofNullable(rawValue).orElse("");
    }

    /**
     * Devuelve el valor de una celda evaluada.
     * 
     * @param cellRawValue
     *            : valor de la celda.
     * @param sheetName
     *            : nombre de la hoja donde se encuentra la celda.
     * @return String con el valor de la celda evaluada.
     */
    private String evaluateCellRawValue(String cellRawValue, String sheetName, String col,
            Integer row) {
        CellInterpreter cellInterpreter = new CellInterpreterImpl(sheetName).setCol(col)
                .setRow(row);
        return cellInterpreter.getCellValue(cellRawValue, this);
    }

    @Override
    public String getCellEvaluatedValue(String sheet, String col, Integer row)
            throws InvalidCellIdentifierException {
        String cellRawValue = book.getCellRawValue(sheet, col, row);
        return this.evaluateCellRawValue(returnRawValue(cellRawValue), sheet, col, row);
    }

}
