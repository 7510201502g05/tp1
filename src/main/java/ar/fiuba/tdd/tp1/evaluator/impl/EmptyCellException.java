package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;

public class EmptyCellException extends CellEvaluatorException {

    private static final long serialVersionUID = 1L;

    public EmptyCellException(String message) {
        super(message);
    }

}
