package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;

@SuppressWarnings("serial")
public class CycleDetectedException extends CellEvaluatorException {
    public CycleDetectedException(String msg) {
        super(msg);
    }
}
