package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.SimpleGraph;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

public class StringVertexSimpleGraph implements SimpleGraph<String> {
    private DirectedGraph<String, DefaultEdge> graph = new SimpleDirectedGraph<>(DefaultEdge.class);

    @Override
    public SimpleGraph<String> addVertex(String vertexName) {
        graph.addVertex(vertexName);
        return this;
    }

    @Override
    public SimpleGraph<String> addEdge(String orgVertexName, String dstVertexName) {
        graph.addEdge(orgVertexName, dstVertexName);
        return this;
    }

    @Override
    public boolean cycleDetected() {
        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<>(graph);
        return cycleDetector.detectCycles();
    }

    @Override
    public boolean noCyclesFound() {
        return cycleDetected() == false;
    }

    public String toString() {
        return graph.toString();
    }
}
