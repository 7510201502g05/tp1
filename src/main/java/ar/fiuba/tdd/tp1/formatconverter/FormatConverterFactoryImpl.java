package ar.fiuba.tdd.tp1.formatconverter;

import java.util.HashMap;

public class FormatConverterFactoryImpl implements FormatConverterFactory {

    private HashMap<String, FormatConverter> formatMap;
    
    public FormatConverterFactoryImpl() {
        this.formatMap = new HashMap<String, FormatConverter>();
        this.formatMap.put("Number", new NumberConverter());
        this.formatMap.put("String", new StringConverter());
        this.formatMap.put("Money", new MoneyConverter());
        this.formatMap.put("Date", new DateConverter());
    }
    
    @Override
    public FormatConverter create(String type) {
        return this.formatMap.get(type);
    }
    

}
