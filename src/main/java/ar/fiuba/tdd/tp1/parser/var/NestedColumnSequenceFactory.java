package ar.fiuba.tdd.tp1.parser.var;


public class NestedColumnSequenceFactory implements ColumnSequenceFactory{
    @Override
    public ColumnSequence create(String col) {
        return NestedColumnSequence.create(col);
    }

    // @Override
    // public ColumnSequence create(String col, String first, String last) {
    // return NestedColumnSequence.create(col, first, last);
    // }
    //
    // @Override
    // public ColumnSequence create(String first, String last) {
    // return new NestedColumnSequence(first, last);
    // }
}
