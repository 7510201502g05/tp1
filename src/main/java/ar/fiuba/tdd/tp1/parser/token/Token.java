package ar.fiuba.tdd.tp1.parser.token;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Representa un elemento a parsear.
 * 
 * @author martin.zaragoza
 *
 */
public class Token {
    public final int tokenType;
    public final String sequence;
    public final int pos;

    public boolean typeEquals(Integer tt) {
        return Optional.ofNullable(tt).orElse(TokenType.NULL_TYPE).equals(tokenType);
    }

    /**
     * Crea una nueva instancia de Token.
     * 
     * @param type
     *            - tipo de token.
     * @param sequence
     *            - secuencia de string que representa.
     * @param pos
     *            - posicion dentro del string original.
     */
    public Token(int type, String sequence, int pos) {
        super();
        this.tokenType = type;
        this.sequence = sequence;
        this.pos = pos;
    }

    /**
     * Crea una nueva instancia de Token.
     * 
     * @param token
     *            - tipo de token.
     * @param sequence
     *            - secuencia de string que representa.
     */
    public Token(int token, String sequence) {
        super();
        this.tokenType = token;
        this.sequence = sequence;
        this.pos = -1;
    }

    public String typeAndSequence() {
        return tokenType + "->" + sequence;
    }

    @Override
    public String toString() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tokenType", tokenType);
        map.put("sequence", sequence);
        map.put("pos", pos);
        return map.toString();
    }
}