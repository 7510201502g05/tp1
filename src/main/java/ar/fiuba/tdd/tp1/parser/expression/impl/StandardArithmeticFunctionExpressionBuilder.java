package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.ArithmeticOperation;
import ar.fiuba.tdd.tp1.parser.expression.FunctionExpressionBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Constructor de funciones aritmeticas estandar. Construye expresiones de funciones aritmeticas que
 * entenderan: sin cos tan asin acos atan sqrt exp ln log2.
 * 
 * @author martin.zaragoza
 *
 */
public class StandardArithmeticFunctionExpressionBuilder implements FunctionExpressionBuilder {
    private static Map<String, ArithmeticOperation> operations = new HashMap<String, ArithmeticOperation>();

    static {
        operations.put("sin", Math::sin);
        operations.put("cos", Math::cos);
        operations.put("tan", Math::tan);
        operations.put("asin", Math::asin);
        operations.put("acos", Math::acos);
        operations.put("atan", Math::atan);
        operations.put("sqrt", Math::sqrt);
        operations.put("exp", Math::exp);
        operations.put("ln", Math::log);
        operations.put("log", val -> {
                return Math.log(val) * 0.43429448190325182765;
            });
        operations.put("log2", val -> {
                return Math.log(val) * 1.442695040888963407360;
            });
    }

    @Override
    public FunctionExpression build() {
        FunctionExpression functionExpressionNode = new FunctionExpression();
        functionExpressionNode.addOperations(operations);
        return functionExpressionNode;
    }

    public static StandardArithmeticFunctionExpressionBuilder newInstance() {
        return new StandardArithmeticFunctionExpressionBuilder();
    }
}
