package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserException;
import ar.fiuba.tdd.tp1.parser.TokenInterpreter;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.SequenceOperationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.VariableExpression;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.util.DefaultValueMap;
import ar.fiuba.tdd.tp1.util.VarRef;

public class ValueTokenInterpreterBuilder extends TokenInterpreterBuilder {

    public ValueTokenInterpreterBuilder(Parser parser) {
        super(parser);
    }

    @Override
    public DefaultValueMap<Integer, TokenInterpreter> build() {
        argumentTokenInterpreters = new DefaultValueMap<>();

        putValueNumber();
        putValueVariable();
        putValuePlusminus();
        putArgumentDefaultValue();

        return argumentTokenInterpreters;
    }

    protected void putValueNumber() {
        argumentTokenInterpreters.put(tokType.number(), params -> {
                print("value -> NUMBER");
                TokenConsumer tokenConsumer = (TokenConsumer) params[0];
                Expression expr = ConstantExpression.create(tokenConsumer.getCurrentTokenSequence());
                nextToken();
                return expr;
            });
    }

    protected void putValueVariable() {
        argumentTokenInterpreters.put(tokType.variable(),params -> {
                print("value -> VARIABLE");
                String strVar = VarRef.get().absVarReferenceBuilder()
                        .build(getDefaultPointer(), getSequenceAndDiscardToken());
                VariableExpression expr = new VariableExpression(strVar);
                addReferencedVariable(strVar);
                return expr;
            });
    }

    protected void putValuePlusminus() {
        argumentTokenInterpreters.put(tokType.plusminus(), params -> {
                print("value -> PLUSMINUS value");
                boolean positive = this.getSequenceAndDiscardToken().trim().equals("+");
                return new SequenceOperationExpression(value(), positive);
            });
    }

    protected void putArgumentDefaultValue() {
        this.argumentTokenInterpreters.setDefaultValue((params) -> {
                throw new ParserException("Simbolo encontrado inesperado "
                        + getCurrentTokenSequence() + " found");
            });
    }
}
