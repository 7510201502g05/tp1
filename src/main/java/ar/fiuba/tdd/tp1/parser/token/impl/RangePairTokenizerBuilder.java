package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.TokenizerBuilder;
import ar.fiuba.tdd.tp1.util.VarRef;

public class RangePairTokenizerBuilder implements TokenizerBuilder {
    private static final TokenType rptokentype = TokenType.get();

    private static final Integer rangeTermId = rptokentype.rangeTerm();
    private static final Integer rangeFunction = rptokentype.rangeFunction();
    private static final Integer pairFunction = rptokentype.pairFunction();
    private static final Integer comma = rptokentype.comma();
    private static final int openBracket = rptokentype.openBracket();
    private static final int closeBracket = rptokentype.closeBracket();
    private static final Integer misc = rptokentype.misc();
    private static final Integer equals = rptokentype.equals();

    private static final String absoluteRangeRegex = VarRef.get().absoluteRangeRegex();
    private static final String relativeRangeRegex = VarRef.get().relativeRangeRegex();
    private static final String miscRegex = VarRef.get().miscRegex();

    private static ExcelTokenizerBuilder excelTokenizerBuilder = new ExcelTokenizerBuilder();

    public Tokenizer build() {
        Tokenizer tokenizer = excelTokenizerBuilder.build();

        tokenizer.stack("\\(", openBracket);
        tokenizer.stack("\\)", closeBracket);
        tokenizer.stack(",", comma);
        tokenizer.stack(relativeRangeRegex, rangeTermId);
        tokenizer.stack(absoluteRangeRegex, rangeTermId);

        tokenizer.stack("CONCAT", pairFunction);
        tokenizer.stack("AVERAGE|MAX|MIN", rangeFunction);
        tokenizer.stack("=", equals);

        tokenizer.add(miscRegex, misc);

        return tokenizer;
    }
}
