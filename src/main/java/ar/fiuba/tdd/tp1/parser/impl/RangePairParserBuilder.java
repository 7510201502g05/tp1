package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.impl.RangePairTokenizerBuilder;

public class RangePairParserBuilder implements ParserBuilder {
    private static final RangePairTokenizerBuilder rangePairTokenizerBuilder = new RangePairTokenizerBuilder();

    @Override
    public Parser build() {
        Tokenizer tokenizer = rangePairTokenizerBuilder.build();
        return new RangePairParser(tokenizer);
    }

}
