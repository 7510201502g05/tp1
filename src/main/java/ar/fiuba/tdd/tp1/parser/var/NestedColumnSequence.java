package ar.fiuba.tdd.tp1.parser.var;

import java.util.Arrays;
import java.util.Optional;

/**
 * Representa una secuencia de columnas del tipo A,B,...,Y,Z,AA,AB,...,AZ,BA.
 * 
 * @author martin
 *
 */
public class NestedColumnSequence implements ColumnSequence {
    private ColumnSequence thisSequence = null;
    private ColumnSequence parentSequence = null;
    private static final ColumnSequence NULL_COLUMN_SEQ = new NullColumnSequence();

    protected ColumnSequence getThisSequence() {
        return thisSequence;
    }

    protected void setThisSequence(ColumnSequence thisSequence) {
        this.thisSequence = thisSequence;
    }

    protected ColumnSequence getParentSequence() {
        return parentSequence;
    }

    protected void setParentSequence(ColumnSequence parentSequence) {
        this.parentSequence = parentSequence;
    }

    /**
     * Crea una secuencia a partir de una columna [A,Z].
     * 
     * @param col
     *            - Valor de columna (A-Z).
     */
    protected NestedColumnSequence(String col) {
        thisSequence = new SimpleColumnSequence(col);
    }

    /**
     * Crea una instancia nueva con limites A>...>Z>AA>AB a partir de una columna inicial.
     * 
     * @param beginCol
     *            - Columna de inicio de secuencia.
     * @return Nuevo secuenciador de columnas.
     */
    public static NestedColumnSequence create(String beginCol) {
        NestedColumnSequence nestedColumnSequence = new NestedColumnSequence(
                beginCol.substring(beginCol.length() - 1));
        return nextWhileColNotEquals(beginCol, nestedColumnSequence);
    }

    public static NestedColumnSequence create(String beginCol, String first, String last) {
        NestedColumnSequence nestedColumnSequence = new NestedColumnSequence(
                beginCol.substring(beginCol.length() - 1), first, last);
        return nextWhileColNotEquals(beginCol, nestedColumnSequence);
    }

    private static NestedColumnSequence nextWhileColNotEquals(String col,
            NestedColumnSequence nestedColumnSequence) {
        while (!col.equals(nestedColumnSequence.getCurrent())) {
            nestedColumnSequence.next();
        }

        return nestedColumnSequence;
    }

    /**
     * Crea una secuencia a partir de una columna y los valores limite de la secuencia.
     * 
     * @param col
     *            - Columna inicial [first,last].
     * @param first
     *            - Primer elemento de secuencia.
     * @param last
     *            - Ultimo elemento de secuencia.
     */
    protected NestedColumnSequence(String col, String first, String last) {
        super();
        thisSequence = new SimpleColumnSequence(col, first, last);
    }

    /**
     * Crea una secuencia a partir de los valores limite de la secuencia.
     * 
     * @param first
     *            - Primer elemento de secuencia.
     * @param last
     *            - Ultimo elemento de secuencia.
     */
    protected NestedColumnSequence(String first, String last) {
        super();
        thisSequence = new SimpleColumnSequence(first, last);
    }

    @Override
    public ColumnSequence clearReset() {
        thisSequence.clearReset();
        return this;
    }

    @Override
    public boolean didReset(boolean clearReset) {
        return thisSequence.didReset();
    }

    @Override
    public String getCurrent() {
        return parentOrElse().getCurrent() + thisSequence.getCurrent();
    }

    public String getFirst() {
        return thisSequence.getFirst();
    }

    public String getLast() {
        return thisSequence.getLast();
    }

    public ColumnSequence doReset() {
        thisSequence.doReset();
        parentOrElse().doReset();
        return this;
    }

    private ColumnSequence parentOrElse() {
        return Optional.ofNullable(parentSequence).orElse(NULL_COLUMN_SEQ);
    }

    @Override
    public ColumnSequence next() {
        thisSequence.next();

        if (thisSequence.didReset()) {
            if (parentSequence == null) {
                parentSequence = new NestedColumnSequence(getFirst(), getLast());
            } else {
                parentSequence.next();
            }
        }

        return this;
    }
}