package ar.fiuba.tdd.tp1.parser.expression.impl;

public class InvalidRangeException extends RuntimeException {

    public InvalidRangeException(String string) {
        super(string);
    }

}
