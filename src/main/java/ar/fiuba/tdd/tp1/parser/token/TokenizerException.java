package ar.fiuba.tdd.tp1.parser.token;

public class TokenizerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TokenizerException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

}
