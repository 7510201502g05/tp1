package ar.fiuba.tdd.tp1.parser.expression.impl.pair;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ConcatExpression extends PairFunctionExpression {

    @Override
    public Double getValueAsDouble(Context context) {
        throw new UnsupportedOperationException(
                "Imposible obtener valor numerico de concatenacion...");
    }

    @Override
    public String getValueAsString(Context context) {
        List<String> stringValues = new ArrayList<>();
//        this.getExpressions().forEach(expr -> stringValues.add(expr.getValueAsString(context)));
        List<Expression> expressions = this.getExpressions();
        for (Expression expression : expressions) {
            stringValues.add( expression.getValueAsString(context) );
        }
        return StringUtils.joinAllAsString(stringValues);
    }

    @Override
    public PairFunctionExpression copy() {
        ConcatExpression copy = new ConcatExpression();
        this.getExpressions().forEach(expr -> copy.add(expr));
        return copy;
    }
}
