package ar.fiuba.tdd.tp1.parser.token;

import java.util.regex.Pattern;

/**
 * Asocia una expresion regular compilada a un tipo de Token.
 * 
 * @author martin.zaragoza
 *
 */
public class TokenInfo {
    public final Pattern regex;
    public final int token;

    public TokenInfo(String strRegex, int tokenType) {
        this.regex = Pattern.compile("^(" + strRegex + ")");
        this.token = tokenType;
    }

    public String toString() {
        return token + ":" + regex;
    }
}// TokenInfo
