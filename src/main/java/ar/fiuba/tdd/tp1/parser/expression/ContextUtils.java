package ar.fiuba.tdd.tp1.parser.expression;

import java.util.Optional;

public class ContextUtils {
    private static class MissingValueOnContextException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public MissingValueOnContextException(String key) {
            super("Valor " + key + " no encontrado en contexto!");
        }
    }

    private static Object getVal(Context context, String key) {
        Object val = Optional.ofNullable(context.get(key)).orElseThrow(() -> new MissingValueOnContextException(key));
        return val;
    }

    public static Double getValueAsDouble(Context context, String key) {
        Object val = getVal(context, key);

        if (val.getClass().getSuperclass().equals(Number.class)) {
            return ((Number) val).doubleValue();
        } else if (val instanceof Expression) {
            return ((Expression) val).getValueAsDouble(context);
        } else if (val instanceof String) {
            return Double.valueOf(val.toString());
        }

        throw new RuntimeException("Imposible obtener el valor de " + key + " como Double!");
    }

    public static String getValueAsString(Context context, String key) {
        Object val = getVal(context, key);

        if (val instanceof Expression) {
            return ((Expression) val).getValueAsString(context);
        } else if (val.getClass().equals(String.class)) {
            return val.toString();
        } else {
            return getValueAsDouble(context, key).toString();
        }
    }
    
    @FunctionalInterface
    public static interface ValueGetter{
        Object get(Context context, String key);
    }
}
