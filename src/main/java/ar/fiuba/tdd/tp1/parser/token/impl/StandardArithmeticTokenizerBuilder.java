package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.TokenizerBuilder;

public class StandardArithmeticTokenizerBuilder implements TokenizerBuilder {
    private static final int NUMBER = TokenType.get().number();
    private static final int VARIABLE = TokenType.get().variable();

    @Override
    public Tokenizer build() {
        Tokenizer tokenizer = new TokenizerImpl();
        String funcs = "sin|cos|tan|asin|acos|atan|sqrt|exp|ln|log|log2";

        tokenizer.add("[+-]", TokenType.get().plusminus());
        tokenizer.add("[*/]", TokenType.get().multdiv());
        tokenizer.add("\\^", TokenType.get().raised());

        tokenizer.add("(" + funcs + ")(?!\\w)", TokenType.get().function());

        tokenizer.add("\\(", TokenType.get().openBracket());
        tokenizer.add("\\)", TokenType.get().closeBracket());
        tokenizer.add("(?:\\d+\\.?|\\.\\d)\\d*(?:[Ee][-+]?\\d+)?", NUMBER);
        tokenizer.add("[a-zA-Z]\\w*", VARIABLE);

        return tokenizer;
    }

}
