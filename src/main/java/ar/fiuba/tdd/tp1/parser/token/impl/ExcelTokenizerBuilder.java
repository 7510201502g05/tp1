package ar.fiuba.tdd.tp1.parser.token.impl;

import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.TokenizerBuilder;
import ar.fiuba.tdd.tp1.util.VarRef;

/**
 * Construye un tokenizador de expresiones de sintaxis similar a las de excel.
 * 
 * @author martin
 *
 */
public class ExcelTokenizerBuilder implements TokenizerBuilder {
    @Override
    public Tokenizer build() {
        Tokenizer tokenizer = new ExcelTokenizer();

        tokenizer.add("[+-]", TokenType.get().plusminus());
        tokenizer.add("[*/]", TokenType.get().multdiv());
        tokenizer.add(VarRef.get().absoluteRegex(), TokenType.get().variable());
        tokenizer.add(VarRef.get().relativeRegex(), TokenType.get().variable());
        tokenizer.add(VarRef.get().doubleRegex(), TokenType.get().number());
        tokenizer.stack("=", TokenType.get().equals());

        return tokenizer;
    }
}
