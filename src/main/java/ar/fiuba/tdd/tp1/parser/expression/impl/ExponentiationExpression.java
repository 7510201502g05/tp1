package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

/**
 * Representa expresion de exponenciacion o potencia.
 * 
 * @author martin
 *
 */
public class ExponentiationExpression implements Expression {
    private Expression base;
    private Expression exponent;

    public ExponentiationExpression(Expression base, Expression exponent) {
        super();
        this.base = base;
        this.exponent = exponent;
    }

    @Override
    public Double getValueAsDouble(Context context) {
        return Math.pow(base.getValueAsDouble(context), exponent.getValueAsDouble(context));
    }

}
