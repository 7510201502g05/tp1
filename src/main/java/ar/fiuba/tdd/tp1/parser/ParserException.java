package ar.fiuba.tdd.tp1.parser;

public class ParserException extends RuntimeException {
    private static final long serialVersionUID = -1009747984332258423L;

    public ParserException(String message) {
        super(message);
    }
}
