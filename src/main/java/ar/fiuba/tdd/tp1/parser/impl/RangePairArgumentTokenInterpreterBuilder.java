package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.ParserException;
import ar.fiuba.tdd.tp1.parser.TokenInterpreter;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.RangeExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.RangeOperationExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.pair.PairFunctionExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.pair.PairFunctionLibrary;
import ar.fiuba.tdd.tp1.parser.token.TokenConsumer;
import ar.fiuba.tdd.tp1.util.DefaultValueMap;

import java.util.List;
import java.util.Optional;

public class RangePairArgumentTokenInterpreterBuilder extends ArgumentTokenInterpreterBuilder {

    public RangePairArgumentTokenInterpreterBuilder(RangePairParser parser) {
        super(parser);
    }

    @Override
    public DefaultValueMap<Integer, TokenInterpreter> build() {
        final DefaultValueMap<Integer, TokenInterpreter> build = super.build();

        this.putArgumentPairFunction();
        this.putArgumentMisc();
        this.putArgumentRangeFunction();

        return build;
    }

    protected void putArgumentPairFunction() {
        argumentTokenInterpreters.put(tokType.pairFunction(),params -> {
                print("argument -> PAIR_FUNCTION OPEN_BRACKET expression COMMA expression CLOSE_BRACKET");
                final String strPairFunction = getSequenceAndDiscardToken();
                final PairFunctionExpression pairFunction = PairFunctionLibrary
                        .getInstance().get(strPairFunction);

                Optional.of(getCurrentTokenType())
                        .filter(tt -> tt.equals(tokType.openBracket()))
                        .orElseThrow(
                                () -> new ParserException(
                                        "Se esperaba apertura de parentesis en la expresion de funcion par"));
                nextToken();

                addPairFunctionArguments(strPairFunction, pairFunction);

                return pairFunction;
            });
    }

    private void addPairFunctionArguments(final String strPairFunction,
            final PairFunctionExpression pairFunction) {
        while (true) {
            if (getCurrentTokenType().equals(tokType.equals())) {
                nextToken();
            }
            pairFunction.add(this.expression());
            if (getCurrentTokenType().equals(tokType.closeBracket())) {
                break;
            }
            checkForInvalidTokens(strPairFunction);
        }
    }

    private void checkForInvalidTokens(final String strPairFunction) {
        Optional.of(getCurrentTokenType())
                .filter(tt -> tt.equals(tokType.comma()))
                .orElseThrow(
                        () -> new ParserException(
                                "Se esperaba una comma en la expresion de funcion par"));
        nextToken();
        if (getCurrentTokenType().equals(tokType.epsilon())) {
            throw new ParserException(
                    "Simbolo inesperado encontrado en construccion de "
                            + strPairFunction);
        }
    }

    protected void putArgumentMisc() {
        argumentTokenInterpreters.put(tokType.misc(), params -> {
                print("argument -> MISC");
                String misc = getSequenceAndDiscardToken();
                return new ConstantExpression(misc);
            });
    }

    protected void putArgumentRangeFunction() {
        argumentTokenInterpreters.put(tokType.rangeFunction(), params -> {
                print("argument -> RANGE_FUNCTION OPEN_BRACKET RANGE_TERM CLOSE_BRACKET");
                final String strRangeFunction = getSequenceAndDiscardToken();

                getSequenceAndDiscardToken();
                final String strRange = getSequenceAndDiscardToken();
                final RangeExpression expr = new RangeExpression(strRange,
                        getDefaultPointer());
                Optional.of(getTokenConsumer().getCurrentTokenType())
                        .filter(tt -> tt.equals(tokType.closeBracket()))
                        .orElseThrow(
                                () -> new ParserException(
                                        "Se esperaba cierre de parentesis despues de rango!"));
                nextToken();

                addReferencedVariables(expr.buildRangeSequence().asList());
                return new RangeOperationExpression(expr, strRangeFunction);
            });
    }

}
