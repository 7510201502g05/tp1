package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.impl.ExcelTokenizerBuilder;

/**
 * Construye un parser estandard para expresiones basicas de sintaxis similar a excel.
 * 
 * @author martin
 *
 */
public class StandardExcelParserBuilder implements ParserBuilder {

    @Override
    public Parser build() {
        Tokenizer tokenizer = new ExcelTokenizerBuilder().build();
        Parser parser = new ParserImpl(tokenizer);
        return parser;
    }

}
