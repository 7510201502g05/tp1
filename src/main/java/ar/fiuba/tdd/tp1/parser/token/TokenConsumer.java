package ar.fiuba.tdd.tp1.parser.token;

/**
 * Representa un consumidor de tokens.
 * 
 * @author martin.zaragoza
 *
 */
public interface TokenConsumer {
    /**
     * Retorna el token actual.
     * 
     * @return token actual, {@link TokenConsumer#getDefault()} si el contenedor esta vacio.
     */
    public Token getCurrent();

    /**
     * Descarta el token actual del contenedor y retorna this.
     * 
     * @return this.
     */
    public TokenConsumer next();

    /**
     * Retorna true si el container esta vacio.
     * 
     * @return true si el container esta vacio.
     */
    public boolean empty();

    /**
     * Agrega un token nuevo al final del consumidor.
     * 
     * @param token
     *            - token a agregar.
     */
    public void add(Token token);

    /**
     * Establece el token por defecto a retornar si el consumidor se vacio y se solicita
     * {@link TokenConsumer#getCurrent()}.
     * 
     * @param token
     *            - Token por defecto a establecer
     * @return this.
     */
    public TokenConsumer setDefault(Token token);

    /**
     * Retorna el token por defecto asignado al consumidor.
     * 
     * @return token por defecto asignado al consumidor.
     */
    public Token getDefault();

    /**
     * Obtiene el string secuencia del token actual.
     * 
     * @return string secuencia del token actual.
     */
    default String getCurrentTokenSequence() {
        return this.getCurrent().sequence;
    }

    /**
     * Retorna el tipo de token actual.
     * 
     * @return tipo de token actual.
     */
    default Integer getCurrentTokenType() {
        return this.getCurrent().tokenType;
    }

    /**
     * Retorna posicion del token actual dentro del string donde se encontro.
     * 
     * @return posicion del token actual dentro del string donde se encontro.
     */
    default Integer getCurrentTokenPos() {
        return this.getCurrent().pos;
    }

    /**
     * Retorna true si el tipo se encuentra en el conjunto de tokens.
     * 
     * @param tokenType
     *            - Tipo a verificar si se encuentra.
     * @return True en caso que el tipo este presente.
     */
    public boolean containsType(Integer tokenType);
}
