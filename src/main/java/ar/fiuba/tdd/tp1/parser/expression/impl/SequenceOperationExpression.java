package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.BinaryOperation;
import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Representa una secuencia de operaciones binarias.
 * 
 * @author martin.zaragoza
 *
 */
public class SequenceOperationExpression implements Expression {
    private boolean positive = true;
    private static final Map<String, BinaryOperation> OPERATIONS = new HashMap<>();
    private List<Operand> operands = new ArrayList<>();

    static {
        OPERATIONS.put("+", BinaryOperation::add);
        OPERATIONS.put("-", BinaryOperation::subtract);
        OPERATIONS.put("/", BinaryOperation::divide);
        OPERATIONS.put("*", BinaryOperation::multiply);
    }

    static class Operand {
        private final Expression term;
        private final BinaryOperation operation;

        public Operand(Expression term, BinaryOperation operation) {
            super();
            this.term = term;
            this.operation = operation;
        }
    }

    /**
     * Agrega un operando a la secuencia. Si el operando a agregar es el primero de la secuencia, su
     * simbolo de operacion es ignorado.
     * 
     * @param term
     *            - Termino a agregar.
     * @param strOperation
     *            - Tipo de operacion: "+" , "-" , "*" , "/".
     * @return this.
     */
    public SequenceOperationExpression addOperand(Expression term, String strOperation) {
        BinaryOperation operation = getOperation(strOperation);
        Operand operand = new Operand(term, operation);
        this.operands.add(operand);
        return this;
    }

    /**
     * Agrega un operando como suma a la secuencia.
     * 
     * @param term
     *            - Termino a agregar.
     * @return this.
     */
    public SequenceOperationExpression addOperand(Expression term) {
        return this.addOperand(term, "+");
    }

    private BinaryOperation getOperation(String strOperation) {
        return OPERATIONS.get(strOperation);
    }

    /**
     * Crea una instancia de operacion en secuencia.
     * 
     * @param initialTerm
     *            - Termino inicial.
     * @param positive
     *            - False si se desea que el resultado final de la operacion sea multiplicada por
     *            -1.
     */
    public SequenceOperationExpression(Expression initialTerm, boolean positive) {
        this.addOperand(initialTerm);
        this.setPositive(positive);
    }

    /**
     * Crea una instancia de operacion en secuencia.
     * 
     * @param initialTerm
     *            - Termino inicial.
     */
    public SequenceOperationExpression(Expression initialTerm) {
        this.addOperand(initialTerm);
    }

    /**
     * Crea una instancia de operacion en secuencia vacia.
     */
    public SequenceOperationExpression() {
        super();
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    @Override
    public Double getValueAsDouble(Context context) {
        if (operands.isEmpty()) {
            return 0.0;
        } else {
            Double res = 0.0;
            Iterator<Operand> iterator = operands.iterator();
            res += iterator.next().term.getValueAsDouble(context);

            while (iterator.hasNext()) {
                Operand operand = iterator.next();
                Double termValue = operand.term.getValueAsDouble(context);
                res = operand.operation.run(res, termValue);
            }

            return positive ? res : res * (-1);
        }
    }
}// SequenceOperationImpl
