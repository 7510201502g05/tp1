package ar.fiuba.tdd.tp1.parser.var;

/**
 * Representa una secuencia de columnas.
 * 
 * @author martin
 *
 */
public interface ColumnSequence {
    default String getFirst() {
        return "";
    }

    default String getLast() {
        return "";
    }

    /**
     * Reinicia la secuencia.
     * 
     * @return this.
     */
    default ColumnSequence doReset() {
        return this;
    }

    /**
     * Limpia el reset.
     * 
     * @return this.
     */
    public default ColumnSequence clearReset() {
        return this;
    }

    /**
     * Retorna true si la secuencia ha reseteado. Limpia el reset luego.
     * 
     * @return true si la secuencia ha reseteado.
     */
    default boolean didReset() {
        return didReset(true);
    }

    /**
     * Retorna true si la secuencia ha reseteado. Limpia el reset luego.
     * 
     * @param clearReset
     *            - true si se desea limpiar el reset luego de preguntarlo.
     * @return true si la secuencia ha reseteado.
     */
    public abstract boolean didReset(boolean clearReset);

    /**
     * Retorna la columna actual o corriente.
     * 
     * @return columna actual o corriente.
     */
    public abstract String getCurrent();

    /**
     * Mueve cursor a siguiente columna en secuencia.
     * 
     * @return this.
     */
    public default ColumnSequence next() {
        return this;
    }

}