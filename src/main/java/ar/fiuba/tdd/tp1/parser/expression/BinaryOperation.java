package ar.fiuba.tdd.tp1.parser.expression;

/**
 * Representa una operacion binaria entre valores Double.
 * 
 * @author martin
 *
 */
@FunctionalInterface
public interface BinaryOperation {
    public static Double add(Double left, Double right) {
        return left + right;
    }

    public static Double subtract(Double left, Double right) {
        return left - right;
    }

    public static Double multiply(Double left, Double right) {
        return left * right;
    }

    public static Double divide(Double left, Double right) {
        return left / right;
    }

    public Double run(Double left, Double right);
}
