package ar.fiuba.tdd.tp1.importer;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;

public interface BookImporter {

    public void importFromFile(SpreadSheetDriverImpl driver, String filePath, String fileName, String bookName);
    
}
