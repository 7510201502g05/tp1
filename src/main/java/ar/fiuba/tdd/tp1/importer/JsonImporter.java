package ar.fiuba.tdd.tp1.importer;

import com.google.gson.Gson;

import ar.fiuba.tdd.tp1.acceptance.driver.ExistingBookFoundException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.exceptions.fileio.ErrorParsingJsonFileException;
import ar.fiuba.tdd.tp1.model.modelcontainer.SpreadSheetModel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class JsonImporter implements BookImporter {

    @Override
    public void importFromFile(SpreadSheetDriverImpl driver, String filePath, String fileName, String bookName) {
        FileInputStream inputFile = null;
        SpreadSheetModel ssModel = null;
        List<String> sheetNames = new ArrayList<String>();

        ssModel = processInputFile(filePath, fileName, inputFile, ssModel);

        if (ssModel != null) {
            createSpreadSheetFromModel(driver, ssModel, sheetNames, bookName);
        } else {
            throw new ErrorParsingJsonFileException(
                    "No se pudo importar el archivo dado que esta corrupto.");
        }
    }

    private void createSpreadSheetFromModel(SpreadSheetDriverImpl driver, SpreadSheetModel ssModel,
            List<String> sheetNames, String bookName) {
        
        if (!driver.workBooksNames().contains(bookName)) {
            driver.createNewWorkBookNamed(bookName);
        }
        
        ssModel.getCells().forEach((cell) -> {
                if (!driver.workSheetNamesFor(bookName).contains(cell.getSheetName())) {
                    sheetNames.add(cell.getSheetName());
                    driver.createNewWorkSheetNamed(bookName, cell.getSheetName());
                }
                cell.addCellToSpreadSheet(driver, bookName);
            }
        );
    }

    private SpreadSheetModel processInputFile(String filePath, String fileName,
            FileInputStream inputFile, SpreadSheetModel ssModel) {
        String fileLine;
        try {
            inputFile = new FileInputStream(filePath + fileName);
            InputStreamReader reader = new InputStreamReader(inputFile, Charset.forName("UTF-8"));
            BufferedReader br = new BufferedReader(reader);

            while ((fileLine = br.readLine()) != null) {
                Gson gson = new Gson();
                ssModel = gson.fromJson(fileLine, SpreadSheetModel.class);
            }
            br.close();
            inputFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ssModel;
    }
}
