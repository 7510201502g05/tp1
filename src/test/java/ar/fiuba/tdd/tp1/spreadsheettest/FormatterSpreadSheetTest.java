package ar.fiuba.tdd.tp1.spreadsheettest;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.format.formatter.FNumber;
import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.Money;
import ar.fiuba.tdd.tp1.spreadsheet.FormatterSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheetImpl;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class FormatterSpreadSheetTest {

    public final FormatterSpreadSheet crpFormatterBookImpl = new FormatterSpreadSheet();

    @Test
    public void createSheet() {
        crpFormatterBookImpl.createSheet("2");
        assertTrue(crpFormatterBookImpl.existsSheet("2"));
    }

    @Test
    public void getNameSheet() {
        crpFormatterBookImpl.createSheet("2");
        List<String> list = crpFormatterBookImpl.getNamesSheet();
        assertEquals(list.get(0), "2");
    }

    @Test
    public void createAndRenameSheet() {
        crpFormatterBookImpl.createSheet("2").renameSheet("2", "Hojita");
        assertTrue(crpFormatterBookImpl.existsSheet("Hojita"));
    }

    @Test
    public void createAndRenameSheet2() {
        crpFormatterBookImpl.createSheet("2").renameSheet("2", "Hojita");
        assertFalse(crpFormatterBookImpl.existsSheet("2"));
    }

    @Test
    public void createAndRemoveSheet() {
        crpFormatterBookImpl.createSheet("2").removeSheet("2");
        assertFalse(crpFormatterBookImpl.existsSheet("2"));
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createAndRenameSheetWithOtherNameSheet() {
        crpFormatterBookImpl.createSheet("2").createSheet("1");
        crpFormatterBookImpl.renameSheet("1", "2");
    }

    @Test(expected = ExistingSheetFoundException.class)
    public void createSheetWithOtherNameSheet() {
        crpFormatterBookImpl.createSheet("2").createSheet("2");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRemoveSheet() {
        crpFormatterBookImpl.removeSheet("21");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidRenameSheet() {
        crpFormatterBookImpl.renameSheet("2", "Hojita");
    }

    @Test(expected = InvalidSheetNameException.class)
    public void invalidCreateSheet() {
        crpFormatterBookImpl.createSheet("");
    }

    @Test
    public void getEmptyCell() {
        try {
            crpFormatterBookImpl.createSheet("1");
            assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"),
                    (new FString()).format("2"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidGetCellBecauseNotExistsSheet() throws InvalidCellIdentifierException {
        crpFormatterBookImpl.getCellRawValue("1", "A", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        crpFormatterBookImpl.createSheet("1").getCellRawValue("1", "A1", 1);
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidGetCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        crpFormatterBookImpl.createSheet("1").getCellRawValue("1", "A", -1);
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidPutCellBecauseNotExistsSheet() {
        crpFormatterBookImpl.putCell("1", "A", 1, new FString());
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidColum() throws InvalidCellIdentifierException {
        crpFormatterBookImpl.createSheet("1").putCell("1", "A1", 1, new FString());

    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellBecauseInvalidRow() throws InvalidCellIdentifierException {
        crpFormatterBookImpl.createSheet("1").putCell("1", "A", -1, new FString());
    }

    @Test
    public void putFormatter() {
        try {
            crpFormatterBookImpl.createSheet("1").putCell("1", "A", 1,
                    new FNumber(1, new FString()));
            assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"),
                    (new FNumber(1, new FString())).format("2"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoPutFormatter() {
        try {
            crpFormatterBookImpl.createSheet("1")
                    .putCell("1", "A", 1, new Money("$", new FNumber(1, new FString())))
                    .putCell("1", "A", 1, new FNumber(1, new FString()));
            crpFormatterBookImpl.undo();
            assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), (new Money(
                    "$", new FNumber(1, new FString()))).format("2"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoPutFormatter() {
        try {
            crpFormatterBookImpl.createSheet("1").putCell("1", "A", 1,
                    new FNumber(1, new FString()));
            crpFormatterBookImpl.undo().redo();
            assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"),
                    (new FNumber(1, new FString())).format("2"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putFormatterAndGetInformation() {
        try {
            crpFormatterBookImpl.createSheet("1").putCell("1", "A", 1,
                    new FNumber(1, new FString()));
            assertEquals(crpFormatterBookImpl.getFormatInformation("1", "A", 1).get("Type"),
                    "Number");
            assertEquals(crpFormatterBookImpl.getFormatInformation("1", "A", 1).get("Decimal"), "1");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void getInformation() {
        try {
            crpFormatterBookImpl.createSheet("1");
            assertEquals(crpFormatterBookImpl.getFormatInformation("1", "A", 1).get("Type"),
                    "String");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }
    
    @Test
    public void putFormatterMoneyAndNumberAndThenUndo() {
        crpFormatterBookImpl.createSheet("1");
        Formatter otherFormatter = crpFormatterBookImpl.getCellRawValue("1", "A", 1);
        crpFormatterBookImpl.putCell("1", "A", 1, new Money("$", otherFormatter));
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2");
        otherFormatter = crpFormatterBookImpl.getCellRawValue("1", "A", 1);
        crpFormatterBookImpl.putCell("1", "A", 1, new FNumber(2, otherFormatter));
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2.00");
        crpFormatterBookImpl.undo();
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2");
        crpFormatterBookImpl.undo();
        String value = crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2");
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "2");
    }
    
    @Test
    public void putFormatterMoneyAndNumberAndThenUndoAndRedo() {
        crpFormatterBookImpl.createSheet("1");
        Formatter otherFormatter = crpFormatterBookImpl.getCellRawValue("1", "A", 1);
        crpFormatterBookImpl.putCell("1", "A", 1, new Money("$", otherFormatter));
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2");
        otherFormatter = crpFormatterBookImpl.getCellRawValue("1", "A", 1);
        crpFormatterBookImpl.putCell("1", "A", 1, new FNumber(2, otherFormatter));
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2.00");
        crpFormatterBookImpl.undo();
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2");
        crpFormatterBookImpl.redo();
        assertEquals(crpFormatterBookImpl.getCellRawValue("1", "A", 1).format("2"), "$ 2.00");
    }

}
