package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.FormatterDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DateMmDdYyyyTest {

    @Test
    public void createDateWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        Formatter formatter = new FormatterDate("MM-dd-yyyy");
        String dateString = formatter.format("" + numberDate);
        assertEquals("01-13-2000", dateString);
    }

    @Test //(expected = InvalidFormatException.class)
    public void createDateWithInvalidDate() {
        Formatter formatter = new FormatterDate("MM-dd-yyyy");
        assertEquals(formatter.format("hola"), "Error:BAD_DATE");
    }

    @Test(expected = NullPointerException.class)
    public void createDateWithNullParametre() {
        new FormatterDate(null);
    }

    @Test
    public void getInformation() {
        Formatter formatter = new FormatterDate("MM-dd-yyyy");
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "Date");
        assertEquals(information.get("Format"), "MM-DD-YYYY");
    }

    @Test
    public void format() {
        DateMmDdYyyy format = new DateMmDdYyyy();
        assertEquals(format.format(), "MM-dd-yyyy");
    }

    @Test
    public void getInformationDateMmDdYyyy() {
        DateMmDdYyyy format = new DateMmDdYyyy();
        assertEquals(format.getInformation(), "MM-DD-YYYY");
    }

}
