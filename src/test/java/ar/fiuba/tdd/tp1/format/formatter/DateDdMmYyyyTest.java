package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.FormatterDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DateDdMmYyyyTest {

    @Test
    public void createDateWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        Formatter formatter = new FormatterDate("dd-MM-yyyy");
        String dateString = formatter.format("" + numberDate);
        assertEquals("13-01-2000", dateString);
    }

    @Test
    public void createDateWithNumber2() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        Formatter formatter = new FormatterDate("dd-mm-yyyy");
        String dateString = formatter.format("" + numberDate);
        assertEquals("13-01-2000", dateString);
    }

    @Test //(expected = InvalidFormatException.class)
    public void createDateWithInvalidDate() {
        Formatter formatter = new FormatterDate("dd-MM-yyyy");
        assertEquals(formatter.format("hola"), "Error:BAD_DATE");
    }

    @Test(expected = NullPointerException.class)
    public void createDateWithNullParametre() {
        new FormatterDate(null);
    }

    @Test
    public void getInformation() {
        Formatter formatter = new FormatterDate("dd-MM-yyyy");
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "Date");
        assertEquals(information.get("Format"), "DD-MM-YYYY");
    }

    @Test
    public void format() {
        DateDdMmYyyy format = new DateDdMmYyyy();
        assertEquals(format.format(), "dd-MM-yyyy");
    }

    @Test
    public void getInformationDateYyyyMmDd() {
        DateDdMmYyyy format = new DateDdMmYyyy();
        assertEquals(format.getInformation(), "DD-MM-YYYY");
    }

}
