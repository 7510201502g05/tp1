package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.Formatter;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;
import ar.fiuba.tdd.tp1.format.formatter.Money;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class MoneyTest {

    @Test
    public void formatterWithTwoDecimal() {
        Formatter otherformatter = new FNumber(2, new FString());
        Formatter formatter = new Money("$", otherformatter);
        assertEquals("$ 2.02", formatter.format("02.020002"));
    }

    @Test
    public void formatterMoney() {
        Formatter formatter = new Money("$", new FString());
        String number = formatter.format("02.020002");
        assertEquals("$ 02.020002", number);
    }

    @Test
    public void formatterWithFiveDecimal() {
        Formatter otherformatter = new FNumber(5, new FString());
        Formatter formatter = new Money("$", otherformatter);
        String value = formatter.format("02.020007");
        assertEquals("$ 2.02001", value);
    }

    @Test //(expected = InvalidFormatException.class)
    public void invalidFormatter() {
        Formatter formatter = new Money("$", new FString());
        assertEquals(formatter.format("Hola"), "Error:BAD_CURRENCY");
    }

    @Test(expected = NullPointerException.class)
    public void createFormatterWithNullParameter() {
        new Money("$", null);
    }

    @Test(expected = InvalidFormatException.class)
    public void createFormatterWithInvalidSymbol() {
        new Money("", new FString());
    }

    @Test
    public void getInformation() {
        Formatter otherformatter = new FNumber(3, new FString());
        Formatter formatter = new Money("$", otherformatter);
        Map<String, String> information = formatter.getInformation();
        assertEquals(information.get("Type"), "Money");
        assertEquals(information.get("Decimal"), "3");
        assertEquals(information.get("Symbol"), "$");
    }
    
    @Test
    public void formatEmptyString() {
        Formatter formatter = new Money("$", new FString());
        assertEquals(formatter.format(""), "");
    }

}
