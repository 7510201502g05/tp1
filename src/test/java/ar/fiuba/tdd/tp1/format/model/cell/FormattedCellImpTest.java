package ar.fiuba.tdd.tp1.format.model.cell;

import ar.fiuba.tdd.tp1.format.formatter.FNumber;
import ar.fiuba.tdd.tp1.format.formatter.FString;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import static org.junit.Assert.*;

public class FormattedCellImpTest {

    @Test
    public void createCell() {
        CrPair id = new CrPair(1, "A");
        FormattedCell<CrPair> cell = new FormattedCellImp<CrPair>(id);
        cell.setFormatter(new FNumber(3, new FString()));
        assertEquals("2.562", (cell.getFormatter()).format("2.5624859"));
    }

    @Test
    public void createCellWithOtherConstructor() {
        CrPair id = new CrPair(1, "A");
        FormattedCell<CrPair> cell = new FormattedCellImp<CrPair>(new FNumber(3, new FString()), id);
        assertEquals("2.562", (cell.getFormatter()).format("2.5624859"));
    }
    
    @Test
    public void createCellAndGetId() {
        CrPair id = new CrPair(1, "A");
        FormattedCell<CrPair> cell = new FormattedCellImp<CrPair>(new FNumber(3, new FString()), id);
        assertEquals(id.getColumn(), cell.getId().getColumn());
        assertEquals(id.getRow(), cell.getId().getRow());
    }

    @Test(expected = NullPointerException.class)
    public void createCellWithNullParameter() {
        new FormattedCellImp<CrPair>(null);
    }

    @Test(expected = NullPointerException.class)
    public void createCellAndSetNullParameter() {
        CrPair id = new CrPair(1, "A");
        FormattedCell<CrPair> cell = new FormattedCellImp<CrPair>(id);
        cell.setFormatter(null);
    }

    @Test
    public void createCellAndGetFormatter() {
        CrPair id = new CrPair(1, "A");
        FormattedCell<CrPair> cell = new FormattedCellImp<CrPair>(id);
        assertEquals(null, cell.getFormatter());
    }

}
