package ar.fiuba.tdd.tp1.format.formatter;

import ar.fiuba.tdd.tp1.format.formatter.FDate;

import org.junit.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.*;

public class FDateTest {

    @Test
    public void createDateWithYearMonthDay() {
        FDate date = new FDate(2000, 1, 13);
        assertEquals(date.year(), 2000);
        assertEquals(date.month(), 1);
        assertEquals(date.day(), 13);
    }

    @Test
    public void createDateWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        FDate date2 = new FDate(numberDate);
        assertEquals(date2.year(), 2000);
        assertEquals(date2.month(), 1);
        assertEquals(date2.day(), 13);
    }

    @Test
    public void createDateWithString() {
        FDate date2 = new FDate("13-01-2000");
        assertEquals(date2.year(), 2000);
        assertEquals(date2.month(), 1);
        assertEquals(date2.day(), 13);

        FDate date = new FDate(2000, 1, 13);
        assertEquals(date.year(), 2000);
        assertEquals(date.month(), 1);
        assertEquals(date.day(), 13);

        assertEquals(date2.date(), date.date());
    }

    @Test
    public void createDateWithStringAndFormat() {
        FDate date2 = new FDate("23-11-1990");
        assertEquals(date2.year(), 1990);
        assertEquals(date2.month(), 11);
        assertEquals(date2.day(), 23);
        assertEquals(date2.getFormattedDate("dd-MM-yyyy"), "23-11-1990");
    }

    @Test
    public void createDateWithStringAndAddDays() {
        FDate date2 = new FDate("23-11-1990");
        long date = date2.date();
        FDate fdate = new FDate(date + 10);
        assertEquals(fdate.getFormattedDate("dd-MM-yyyy"), "03-12-1990");
    }

    @Test
    public void test() {
        LocalDate date = LocalDate.of(2000, 1, 1);
        LocalDate date2 = LocalDate.of(2000, 1, 2);
        long diff = date2.toEpochDay() - date.toEpochDay();
        assertEquals(1, date2.hashCode() - date.hashCode());
        assertEquals(1, diff);
        diff = date2.toEpochDay() + 32;
        assertEquals("2000-02-03", LocalDate.ofEpochDay(diff).toString());
    }

    @Test
    public void getFormattedDate() {
        FDate date = new FDate("13-01-2000");
        assertEquals("01-13-2000", date.getFormattedDate("MM-dd-YYYY"));
    }

    @Test(expected = DateTimeException.class)
    public void invalidDateWithString() {
        new FDate("hola");
    }

    @Test(expected = DateTimeException.class)
    public void invalidDateWithNumber() {
        new FDate(-123);
    }

    @Test(expected = DateTimeException.class)
    public void invalidDate() {
        new FDate(2000, 85, -9);
    }

    @Test
    public void isAValidFormat() {
        FDate date = new FDate("13-01-2000");
        assertTrue(date.isAValidFormat("dd-mm-yyyy"));
        assertFalse(date.isAValidFormat("hola"));
    }

}
