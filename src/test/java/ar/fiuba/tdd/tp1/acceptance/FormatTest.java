package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.BookNotFoundException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetDriverImpl;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.format.formatter.FDate;
import ar.fiuba.tdd.tp1.format.formatter.InvalidFormatException;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FormatTest {

    private SpreadSheetDriverImpl testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetDriverImpl();
        testDriver.createNewWorkBookNamed("book");
    }
    
    @Test (expected = BookNotFoundException.class)
    public void invalidPutCellFormattedBecauseNotExistsBook() {
        testDriver.setCellFormatString("Libro", "Hojas", "A1");
    }

    @Test(expected = SheetNotFoundException.class)
    public void invalidPutCellFormattedBecauseNotExistsSheet() {
        testDriver.setCellFormatString("book", "Hojas", "A1");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellFormmatedBecauseInvalidColum() throws InvalidCellIdentifierException {
        testDriver.createNewWorkSheetNamed("book", "Hoja");
        testDriver.setCellFormatString("book", "Hoja", "1A1");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void invalidPutCellFormmatedBecauseInvalidRow() throws InvalidCellIdentifierException {
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatString("book", "1", "A-1");
    }

    @Test
    public void undoPutNumberAndOperationSubstraction() {
        try {
            testDriver.createNewWorkSheetNamed("book", "1");
            testDriver.setCellValue("book", "1", "A1", "58");
            testDriver.setCellValue("book", "1", "A1", "=3-1");
            testDriver.undo();
            assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "58");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoPutNumberAndFormattedNumberAndMoney() {
        try {
            testDriver.createNewWorkSheetNamed("book", "1");
            testDriver.setCellValue("book", "1", "A1", "58");
            testDriver.setCellFormatMoney("book", "1", "A1", "$");
            testDriver.setCellFormatNumber("book", "1", "A1", 0);
            testDriver.undo();
            assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "$ 58");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoPutOperationSubstractionAndFormatterNumber() {
        try {
            testDriver.createNewWorkSheetNamed("book", "1");
            testDriver.setCellFormatNumber("book", "1", "A1", 3);
            testDriver.setCellValue("book", "1", "A1", "=3-1");
            testDriver.undo();
            testDriver.redo();
            assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "2.000");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void undoAndRedoWithFormattersAndValues() {
        try {
            testDriver.createNewWorkSheetNamed("book", "2");
            testDriver.setCellValue("book", "2", "A1", "5");
            testDriver.setCellFormatNumber("book", "2", "A1", 3);
            testDriver.setCellValue("book", "2", "A2", "=3-1");
            testDriver.undo();
            testDriver.undo();
            testDriver.setCellValue("book", "2", "A2", "hola");
            String value = testDriver.getCellValueAsString("book", "2", "A1");
            assertEquals(testDriver.getCellValueAsString("book", "2", "A1"), "5");
            assertEquals(testDriver.getCellValueAsString("book", "2", "A2"), "hola");
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void putFormatDateYyyyMmDdWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatDate("book", "1", "A1", "yyyy-MM-dd");
        testDriver.setCellValue("book", "1", "A1", "" + numberDate);
        assertEquals("2000-01-13", testDriver.getCellValueAsString("book", "1", "A1"));
    }

    @Test
    public void putFormatDateDdMmYyyyWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatDate("book", "1", "A1", "dd-MM-yyyy");
        testDriver.setCellValue("book", "1", "A1", "" + numberDate);
        assertEquals("13-01-2000", testDriver.getCellValueAsString("book", "1", "A1"));
    }

    @Test
    public void putFormatDateMmDdYyyyWithNumber() {
        FDate date = new FDate(2000, 1, 13);
        long numberDate = date.date();
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatDate("book", "1", "A1", "MM-dd-yyyy");
        testDriver.setCellValue("book", "1", "A1", "" + numberDate);
        assertEquals("01-13-2000", testDriver.getCellValueAsString("book", "1", "A1"));
    }

    @Test
    public void putFomatNumber() {
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatNumber("book", "1", "A1", 1);
        testDriver.setCellValue("book", "1", "A1", "32.34634");
        assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "32.3");
    }

    @Test
    public void putFomatMoney() {
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatMoney("book", "1", "A1", "#");
        testDriver.setCellValue("book", "1", "A1", "32.34634");
        assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "# 32.34634");
    }

    @Test
    public void putFomatString() {
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatString("book", "1", "A1");
        testDriver.setCellValue("book", "1", "A1", "32.34634");
        assertEquals(testDriver.getCellValueAsString("book", "1", "A1"), "32.34634");
    }

    @Test(expected = InvalidFormatException.class)
    public void putInvalidFormatDate() {
        testDriver.createNewWorkSheetNamed("book", "1");
        testDriver.setCellFormatDate("book", "1", "A1", "hola");
    }

}
