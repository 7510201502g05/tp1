package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ContextImpl;
import ar.fiuba.tdd.tp1.parser.impl.ParserImpl;
import ar.fiuba.tdd.tp1.parser.impl.StandardExcelParserBuilder;
import ar.fiuba.tdd.tp1.parser.token.TokenType;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.TokenizerException;
import ar.fiuba.tdd.tp1.parser.token.impl.StandardArithmeticTokenizerBuilder;
import ar.fiuba.tdd.tp1.parser.token.impl.TokenizerImpl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ParserTest {

    private static final double DELTA = 0.000000001;

    @Test
    public void testDummyAdd() {
        System.out.println("testDummyAdd-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new TokenizerImpl();
            tokenizer.add("sin|cos|exp|ln|sqrt", TokenType.get().function()); // function
            tokenizer.add("\\(", TokenType.get().openBracket()); // open bracket
            tokenizer.add("\\)", TokenType.get().closeBracket()); // close bracket
            tokenizer.add("[+-]", TokenType.get().plusminus()); // plus or minus
            tokenizer.add("[*/]", TokenType.get().multdiv()); // mult or divide
            tokenizer.add("\\^", TokenType.get().raised()); // raised
            tokenizer.add("[0-9]+", TokenType.get().number()); // integer number
            tokenizer.add("[a-zA-Z][a-zA-Z0-9_]*", TokenType.get().variable()); // variable

            Parser parser = new ParserImpl(tokenizer);
            parser.parse(" 5 + 4 ");
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParse() {
        System.out.println("testParse-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();
            tokenizer.add("sin|cos|exp|ln|sqrt", TokenType.get().function()); // function
            tokenizer.add("\\(", TokenType.get().openBracket()); // open bracket
            tokenizer.add("\\)", TokenType.get().closeBracket()); // close bracket
            tokenizer.add("[+-]", TokenType.get().plusminus()); // plus or minus
            tokenizer.add("[*/]", TokenType.get().multdiv()); // mult or divide
            tokenizer.add("\\^", TokenType.get().raised()); // raised
            tokenizer.add("[0-9]+", TokenType.get().number()); // integer number
            tokenizer.add("[a-zA-Z][a-zA-Z0-9_]*", TokenType.get().variable()); // variable

            Parser parser = new ParserImpl(tokenizer);
            parser.parse(" sin(x) * (1 + var_12) ");
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseAndGetValue() {
        System.out
                .println("testParseAndGetValue-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            parser.parse("3*2^4 + sqrt(1+3)");
            Expression expr = parser.getEndExpression();
            double result = expr.getValueAsDouble(null);
            System.out.println("value is " + result);
            assertEquals(50.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseUsingVariables() {
        System.out
                .println("testParseUsingVariables-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            Expression expr = parser.parse("sin(pi/2)");
            // expr.accept(new SetVariableVisitor("pi", Math.PI));
            Context variablesContext = new ContextImpl();
            variablesContext.put("pi", Math.PI);
            // parser.setVariableValue("pi", Math.PI);

            double result = expr.getValueAsDouble(variablesContext);
            System.out.println("result is " + result);
            assertEquals(1.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseUsingLotsoVariables() {
        System.out
                .println("testParseUsingLotsoVariables-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            Expression expr = parser.parse("x + y - z * w");
            Context context = new ContextImpl();
            context.put("x", ConstantExpression.create(1.0));
            context.put("y", 2.0);
            context.put("z", 3.0);
            context.put("w", 4.0);

            double result = expr.getValueAsDouble(context);
            System.out.println("result is " + result);
            assertEquals(-9.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseUsingVariablesAndStaticTokenizerConstructor() {
        System.out
                .println("testParseUsingVariablesAndStaticTokenizerConstructor-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            Expression expr = parser.parse("sin(pi/2)");
            Context context = new ContextImpl();
            context.put("pi", Math.PI);

            double result = expr.getValueAsDouble(context);
            System.out.println("result is " + result);
            assertEquals(1.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseComplexExpression() {
        System.out
                .println("testParseComplexExpression-------------------------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            Expression expr = parser.parse("(3^2 -2 +7) / 7");

            double result = expr.getValueAsDouble(null);
            System.out.println("result is " + result);
            assertEquals(2.0, result, DELTA);

            final double PI = 3.1415926535897932384626433832795;
            result = parser.parse("((sin(" + (PI / 2) + ") * 3)^ 2) - (140 / 70) * 3").getValueAsDouble(
                    null);
            assertEquals(3.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseMultipleAdditionsSubstractionsMultsAndDivs() {
        System.out
                .println("testParseMultipleAdditionsSubstractionsMultsAndDivs-------------------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            Expression expr = parser
                    .parse("(12.0 + 0.25 + 0.75 - 0.5 - 0.5 -11.0) * 3 * 2.0 * 2 / 4.0 / 3.0 * 98.98");

            double result = expr.getValueAsDouble(null);
            System.out.println("result is " + result);
            assertEquals(98.98, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testParseNegativeExpressions() {
        System.out.println("testParseNegativeExpressions-----------------------------");

        try {
            Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

            Parser parser = new ParserImpl(tokenizer);
            // ExpressionNode expr = parser
            // .parse("- 30.25 -0.25 * (+2)");

            Expression expr = parser.parse("- 30.25 -0.25 * -1");

            double result = expr.getValueAsDouble(null);
            System.out.println("result is " + result);
            assertEquals(-30.0, result, DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test(expected = ParserException.class)
    public void testParseSinWithNoCloseBrackets() {
        System.out.println("testParseSinWithNoCloseBrackets--------------------------------");

        Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();

        Parser parser = new ParserImpl(tokenizer);
        parser.parse("3.0 * sin(29");

    }

    @Test
    public void testParseExpressionWithWeirdVariables() {
        System.out.println("testParseExpressionWithWeirdVariables----------------------------");

        try {
            Parser parser = new StandardExcelParserBuilder().build();

            Context context = new ContextImpl();
            context.put("1!C22", -17.5);
            context.put("hoja2!A23", 0.5);
            Expression expr = parser.parse("1!C22 + hoja2!A23");

            assertEquals(-17.0, expr.getValueAsDouble(context), DELTA);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test(expected = TokenizerException.class)
    public void testParseExpressionAndFail() {
        System.out.println("testParseExpressionAndFail----------------------------");

        Parser parser = new StandardExcelParserBuilder().build();

        Context context = new ContextImpl();
        context.put("1!C22", -17.5);
        context.put("hoja2!A23", 0.5);
        Expression expr = parser.parse("192.365 + 87.21 - hoja2!A23 * 1!C22 / asd");

        assertEquals(-17.0, expr.getValueAsDouble(context), DELTA);
    }

    @Test(expected = ParserException.class)
    public void testParseIncompleteExpression() {
        System.out.println("testParseIncompleteExpression----------------------------");

        Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();
        Parser parser = new ParserImpl(tokenizer);

        Context context = new ContextImpl();
        Expression expr = parser.parse("192.365 + 87.21 - ");
    }

    @Test(expected = ParserException.class)
    public void testParseIncompleteFunction() {
        System.out.println("testParseIncompleteFunction----------------------------");

        Tokenizer tokenizer = new StandardArithmeticTokenizerBuilder().build();
        Parser parser = new ParserImpl(tokenizer);

        Context context = new ContextImpl();
        Expression expr = parser.parse("192.365 + sin() ");
    }

    @Test
    public void testParseInvalidLietralAddition() {
        System.out.println("testParseInvalidLietralAddition----------------------------");
        Parser parser = new StandardExcelParserBuilder().build();
        parser.parse("3.5 + -1");
    }
    

    
    
}
