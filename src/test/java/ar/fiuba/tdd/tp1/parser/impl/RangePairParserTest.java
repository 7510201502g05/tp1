package ar.fiuba.tdd.tp1.parser.impl;

import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserException;
import ar.fiuba.tdd.tp1.parser.expression.Context;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ConstantExpression;
import ar.fiuba.tdd.tp1.parser.expression.impl.ContextImpl;
import ar.fiuba.tdd.tp1.parser.token.Tokenizer;
import ar.fiuba.tdd.tp1.parser.token.impl.RangePairTokenizerBuilder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RangePairParserTest {
    RangePairTokenizerBuilder rangePairTokenizerBuilder = new RangePairTokenizerBuilder();
    Tokenizer tokenizer = rangePairTokenizerBuilder.build();

    @Test
    public void testParseRangeExpressions() {
        System.out
                .println("testParseRangeExpressions---------------------------------------------");

        RangePairParser parser = new RangePairParser(tokenizer);
        parser.parse("= MAX(G1:G3)");
        parser.parse("=MIN(A2:C4)");
        parser.parse("=AVERAGE(A2:C4)");
    }

    @Test
    public void testParseMixedExpressions() {
        System.out
                .println("testParseMixedExpressions---------------------------------------------");
        RangePairParser parser = new RangePairParser(tokenizer);
        parser.parse("= D23 + MAX(A2:C4)");
        parser.parse("=MIN(A2:C4) - 8.7");
        parser.parse("=AVERAGE(A2:C4) + G5 - 6.7");
    }

    @Test(expected = ParserException.class)
    public void testParseMisc() {
        System.out.println("testParseMisc---------------------------------------------");
        Parser parser = new RangePairParserBuilder().build();
        String toParse = "=hola como estas";
        Expression expr = parser.parse(toParse);
        assertEquals(ConstantExpression.class, expr.getClass());
        assertEquals(toParse, expr.getValueAsString(null));
    }

    @Test
    public void testParseMiscConcat() {
        System.out.println("testParseMiscConcat---------------------------------------------");
        Parser parser = new RangePairParserBuilder().build();
        String toParse = "CONCAT(hola ,como estas)";
        Expression expr = parser.parse(toParse);
        assertEquals("hola como estas", expr.getValueAsString(null));
    }

    @Test
    public void testParseFormulaConcat() {
        System.out.println("testParseFormulaConcat---------------------------------------------");
        Parser parser = new RangePairParserBuilder().build();
        String toParse = "CONCAT( 4.8 + 0.2 ,como estas)";
        Expression expr = parser.parse(toParse);
        assertEquals("5.0como estas", expr.getValueAsString(null));
    }

    @Test
    public void testParseFormulaWithContextConcat() {
        System.out
                .println("testParseFormulaWithContextConcat---------------------------------------------");
        String defaultPointer = "default";
        Parser parser = new RangePairParserBuilder().build().setDefaultPointer(defaultPointer);
        String toParse = "CONCAT( A1 - A2 , 4.5 )";
        Expression expr = parser.parse(toParse);
        Context context = new ContextImpl().put("default!A1", 3.25).put("default!A2", 1.25);
        assertEquals("2.04.5", expr.getValueAsString(context));
    }

    @Test
    public void testParseTernaryConcat() {
        System.out.println("testParseTernaryConcat---------------------------------------------");
        String defaultPointer = "default";
        Parser parser = new RangePairParserBuilder().build().setDefaultPointer(defaultPointer);
        String toParse = "CONCAT( A1 - A2 , 4.5 , =A3 )";
        Expression expr = parser.parse(toParse);
        Context context = new ContextImpl().put("default!A1", 3.25).put("default!A2", 1.25)
                .put("default!A3", 1.25);
        System.out.println(expr.getValueAsString(context));
        assertEquals("2.04.51.25", expr.getValueAsString(context));
    }
}
