package ar.fiuba.tdd.tp1.parser.expression.impl;

import ar.fiuba.tdd.tp1.parser.expression.EvaluationException;

import org.junit.Test;

import static org.junit.Assert.*;

public class VariableExpressionTest {

    @Test
    public void testSetName() {
        VariableExpression variableExpression = new VariableExpression("name1");
        variableExpression.setName("name2");
        assertEquals("name2", variableExpression.getName());
    }

    @Test
    public void testIsValueSet() {
        VariableExpression variableExpression = new VariableExpression("var");
        assertFalse(variableExpression.isValueSet());
        variableExpression.setValue(123.123);
        assertTrue(variableExpression.isValueSet());
    }

    @Test(expected = EvaluationException.class)
    public void testTryEvalEmptyVariable() {
        VariableExpression variableExpression = new VariableExpression("some variable");
        variableExpression.getValueAsDouble(null);
    }
}
