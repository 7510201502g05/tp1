package ar.fiuba.tdd.tp1.parser.var;

import org.junit.Test;

import static org.junit.Assert.*;

public class PointerColumnRowTest {

    @Test
    public void testGetColumn() {
        String cell = "default!C23";
        PointerColumnRow createFromString = PointerColumnRow.createFromString(cell, new NestedColumnSequenceFactory());
        assertEquals("C", createFromString.getColumn());
        assertEquals(new Integer(23), createFromString.getRow());
        assertEquals("default", createFromString.getPointer());

        createFromString.augmentColumn().augmentColumn();
        assertEquals("E", createFromString.getColumn());

        
        assertEquals(createFromString, createFromString.copy());
        cell = "default!E23";
        assertEquals(cell, createFromString.toString());
    }

    @Test
    public void testGetRow() {
        PointerColumnRow createFromString = PointerColumnRow.createFromString("default!C23");
        assertEquals("C", createFromString.getColumn());
        assertEquals(new Integer(23), createFromString.getRow());
        assertEquals("default", createFromString.getPointer());

        createFromString.augmentRow().augmentRow();
        assertEquals(Integer.valueOf(25), createFromString.getRow());
    }

    @Test
    public void testGetColumnNoPointer() {
        String cell = "C23";
        PointerColumnRow createFromString = PointerColumnRow.createFromString(cell, new NestedColumnSequenceFactory());
        assertEquals("C", createFromString.getColumn());
        assertEquals(new Integer(23), createFromString.getRow());
        assertEquals("", createFromString.getPointer());

        createFromString.augmentColumn().augmentColumn();
        assertEquals("E", createFromString.getColumn());

        assertEquals(createFromString, createFromString.copy());
        
        cell = "E23";
        assertEquals(cell, createFromString.toString());
    }

    @Test
    public void testGetRowNoPointer() {
        PointerColumnRow createFromString = PointerColumnRow.createFromString("C23");
        assertEquals("C", createFromString.getColumn());
        assertEquals(new Integer(23), createFromString.getRow());
        assertEquals("", createFromString.getPointer());

        createFromString.augmentRow().augmentRow();
        assertEquals(Integer.valueOf(25), createFromString.getRow());
    }

}
