package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.cell.Cell;
import ar.fiuba.tdd.tp1.model.sheet.Sheet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Esta clase solo se usa para los tests de Commands.
 *
 */
public class BookOnlyTest<DataTypeT> implements Book<String, DataTypeT> {

    private HashMap<String, String> book;

    public BookOnlyTest() {
        book = new HashMap<String, String>();
    }

    @Override
    public Book<String, DataTypeT> createSheet(String name) {
        return null;
    }

    @Override
    public Book<String, DataTypeT> removeSheet(String name) {
        return null;
    }

    @Override
    public Sheet<String, DataTypeT> getSheet(String name) {
        return null;
    }

    @Override
    public Book<String, DataTypeT> putCell(String sheet, String col, int row, DataTypeT rawValue) {
        String key = sheet + col + row;
        book.put(key, (String) rawValue);
        return this;
    }

    @Override
    public Book<String, DataTypeT> clearCell(String sheet, String col, int row) {
        return this;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DataTypeT getCellRawValue(String sheet, String col, int row)
            throws InvalidCellIdentifierException {
        String key = sheet + col + row;
        String result = book.get(key);
        Optional.ofNullable(result).orElseThrow(InvalidCellIdentifierException::new);
        return (DataTypeT) result;
    }

    @Override
    public DataTypeT getCellRawValue(String cellId) {
        return null;
    }

    @Override
    public Book<String, DataTypeT> renameSheet(String oldName, String newName) {
        return null;
    }

    @Override
    public boolean existsSheet(String name) {
        return false;
    }

    @Override
    public List<String> getNamesSheet() {
        return null;
    }

    @Override
    public Book<String, DataTypeT> putSheet(Sheet<String, DataTypeT> sheet) {
        return null;
    }

    @Override
    public HashMap<String, Map<String, Cell<String, DataTypeT>>> getCellsInBook() {
        return null;
    }

}
