package ar.fiuba.tdd.tp1.command;

/**
 * Esta clase solo fue creada para hacer pruebas para CommandHistory. Es una clase que aumenta un
 * valor en dos cuando se ejecuta y disminuye en uno cuando se deshace.
 *
 */
public class CommandOnlyTest implements Command {

    private int value;

    public CommandOnlyTest(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public void execute() {
        this.value++;
        this.value++;
    }

    @Override
    public void undo() {
        this.value--;
    }

}
