package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.ExistingSheetFoundException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.book.CrpBook;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import static org.junit.Assert.*;

public class SheetCreateCommandTest {

    private final Book<CrPair, String> book = new CrpBook<String>();

    @Test
    public void excecuteCommand() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        command.execute();
        assertTrue(book.existsSheet("Hoja"));
    }

    @Test
    public void excecuteAndUndoCommand() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        command.execute();
        command.undo();
        assertFalse(book.existsSheet("Hoja"));
    }

    @Test(expected = SheetNotFoundException.class)
    public void undoCommandWithoutExecute() {
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        command.undo();
        assertFalse(book.existsSheet("Hoja"));
    }

    @Test(expected = NullPointerException.class)
    public void commandWithNullParam() {
        new SheetCreateCommand<CrPair, String>(null, book);
    }

    @Test(expected = InvalidSheetNameException.class)
    public void excecuteCommandWithInvalidName() throws InvalidCellIdentifierException {
        Command command = new SheetCreateCommand<CrPair, String>("", book);
        command.execute();
    }
    
    @Test(expected = ExistingSheetFoundException.class)
    public void excecuteCommandAndExistingSheetFound() throws InvalidCellIdentifierException {
        book.createSheet("Hoja");
        Command command = new SheetCreateCommand<CrPair, String>("Hoja", book);
        command.execute();
    }
    
}
