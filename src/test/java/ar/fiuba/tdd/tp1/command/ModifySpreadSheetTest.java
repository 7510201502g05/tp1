package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheet;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheetImpl;

import org.junit.Test;

import static org.junit.Assert.*;

public class ModifySpreadSheetTest {

    private final ContentSpreadSheet spreadSheet = new ContentSpreadSheetImpl();

    @Test
    public void excecuteCreateSheetAndUndo() {
        spreadSheet.createSheet("Hoja");
        Command command = new ModifySpreadSheetCommand<>(spreadSheet);
        command.execute();
        assertTrue(spreadSheet.existsSheet("Hoja"));
        command.undo();
        assertFalse(spreadSheet.existsSheet("Hoja"));
        command.execute();
        assertTrue(spreadSheet.existsSheet("Hoja"));
    }

    @Test
    public void excecuteRemoveSheetAndUndo() {
        spreadSheet.createSheet("Hoja");
        spreadSheet.removeSheet("Hoja");
        Command command = new ModifySpreadSheetCommand<>(spreadSheet);
        command.execute();
        assertFalse(spreadSheet.existsSheet("Hoja"));
        command.undo();
        assertTrue(spreadSheet.existsSheet("Hoja"));
        command.execute();
        assertFalse(spreadSheet.existsSheet("Hoja"));
    }

    @Test
    public void excecuteRenameSheetAndUndo() {
        spreadSheet.createSheet("Hoja");
        spreadSheet.renameSheet("Hoja", "NuevaHoja");
        Command command = new ModifySpreadSheetCommand<>(spreadSheet);
        command.execute();
        assertFalse(spreadSheet.existsSheet("Hoja"));
        assertTrue(spreadSheet.existsSheet("NuevaHoja"));
        command.undo();
        assertTrue(spreadSheet.existsSheet("Hoja"));
        assertFalse(spreadSheet.existsSheet("NuevaHoja"));
        command.execute();
        assertFalse(spreadSheet.existsSheet("Hoja"));
        assertTrue(spreadSheet.existsSheet("NuevaHoja"));
    }

    public void excecuteSetValueAndUndo() {
        spreadSheet.createSheet("Hoja");
        spreadSheet.putCell("Hoja", "A", 1, "Hola");
        spreadSheet.putCell("Hoja", "A", 1, "Chau");
        Command command = new ModifySpreadSheetCommand<>(spreadSheet);
        command.execute();
        assertEquals(spreadSheet.getCellEvaluatedValue("Hoja", "A", 1), "Chau");
        command.undo();
        assertEquals(spreadSheet.getCellEvaluatedValue("Hoja", "A", 1), "Hola");
        command.execute();
        assertEquals(spreadSheet.getCellEvaluatedValue("Hoja", "A", 1), "Chau");
    }

}
