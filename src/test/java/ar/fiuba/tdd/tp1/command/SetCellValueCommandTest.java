package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheetImpl;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class SetCellValueCommandTest {

    //private final SpreadSheet<String> book = new ContentSpreadSheetImpl();
    private final Book<String, String> book = new BookOnlyTest<String>();

    @Test
    public void excecuteCommand() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            command.execute();
            String result;
            result = book.getCellRawValue("1", "A", 1);
            assertTrue(result.equals("Hola"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void excecuteAndUndoCommand() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            command.execute();
            command.undo();
            assertTrue(book.getCellRawValue("1", "A", 1).equals("Vacio"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

//    @Test
//    public void undoCommandWithoutExecute() {
//        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
//        try {
//            book.putCell("1", "A", 1, "Vacio");
//        } catch (InvalidCellIdentifierException e1) {
//            fail();
//        }
//        command.undo();
//        try {
//            assertTrue(book.getCellRawValue("1", "A", 1).equals("Vacio"));
//        } catch (InvalidCellIdentifierException e) {
//            fail();
//        }
//    }

    @Test(expected = NullPointerException.class)
    public void commandWithNullParam() {
        new SetCellValueCommand<String, String>(null, "1", "A", 1, "Hola");
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void excecuteCommandWithInvalidCell() throws InvalidCellIdentifierException {
        book.createSheet("1");
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", -1, "Hola");
        command.execute();
    }

}
