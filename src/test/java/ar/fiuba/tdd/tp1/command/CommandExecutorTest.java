package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheetImpl;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommandExecutorTest {

    private final Book<String, String> book = new BookOnlyTest<String>();
    private final CommandExecutor commandExecutor = new CommandExecutorImpl();

    @Test
    public void runCommand() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            commandExecutor.runCommand(command);
            String result;
            result = book.getCellRawValue("1", "A", 1);
            assertTrue(result.equals("Hola"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void runCommandAndUndo() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            commandExecutor.runCommand(command);
            commandExecutor.undo();
            assertTrue(book.getCellRawValue("1", "A", 1).equals("Vacio"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void runCommandAndUndoAndRedo() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            commandExecutor.runCommand(command);
            commandExecutor.undo();
            commandExecutor.redo();
            assertTrue(book.getCellRawValue("1", "A", 1).equals("Hola"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void runCommandAndUndoAndrunNewCommandAndRedo() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        Command command2 = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Chau");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            commandExecutor.runCommand(command);
            commandExecutor.undo();
            commandExecutor.runCommand(command2);
            commandExecutor.redo();
            assertTrue(book.getCellRawValue("1", "A", 1).equals("Chau"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test(expected = InvalidCellIdentifierException.class)
    public void runCommandWithInvalidCell() throws InvalidCellIdentifierException {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A5", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        commandExecutor.runCommand(command);
    }
}
