package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.spreadsheet.ContentSpreadSheetImpl;
import ar.fiuba.tdd.tp1.spreadsheet.SpreadSheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommandHistoryTest {

    private final CommandHistory commandHistory = new CommandHistory();
    private final CommandOnlyTest command = new CommandOnlyTest(1);
    private final Book<String, String> book = new BookOnlyTest<String>();

    @Test
    public void addCommandAndUndo() {
        commandHistory.pushCommand(command);
        commandHistory.undo();
        // Deberia disminuir en 1 el valor que tiene command
        assertEquals(command.getValue(), 0);
    }

    @Test
    public void addCommandAndRedo() {
        commandHistory.pushCommand(command);
        commandHistory.redo();
        // Deberia mantener el mismo valor
        assertEquals(command.getValue(), 1);
    }

    @Test
    public void addCommandAndUndoAndRedo() {
        commandHistory.pushCommand(command);
        commandHistory.undo();
        commandHistory.redo();
        // Deberia disminuir en 1 (o sea 0) y luego aumentar en 2
        assertEquals(command.getValue(), 2);
    }

    @Test
    public void excecuteCommandAndClearUndoStack() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            command.execute();
            commandHistory.pushCommand(command);
            commandHistory.clearUndoStack();
            commandHistory.undo();
            String result;
            result = book.getCellRawValue("1", "A", 1);
            assertTrue(result.equals("Hola"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }

    @Test
    public void excecuteCommandAndClearRedoStack() {
        Command command = new SetCellValueCommand<String, String>(book, "1", "A", 1, "Hola");
        try {
            book.putCell("1", "A", 1, "Vacio");
        } catch (InvalidCellIdentifierException e1) {
            fail();
        }
        try {
            command.execute();
            commandHistory.pushCommand(command);
            commandHistory.undo();
            commandHistory.clearRedoStack();
            commandHistory.redo();
            String result = book.getCellRawValue("1", "A", 1);
            assertTrue(result.equals("Vacio"));
        } catch (InvalidCellIdentifierException e) {
            fail();
        }
    }
    
    @Test
    public void redoWithoutCommand() {
        commandHistory.redo();
    }
    
    @Test
    public void undoWithoutCommand() {
        commandHistory.undo();
    }


}
