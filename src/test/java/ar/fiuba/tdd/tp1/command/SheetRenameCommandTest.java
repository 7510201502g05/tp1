package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.exceptions.model.SheetNotFoundException;
import ar.fiuba.tdd.tp1.model.book.Book;
import ar.fiuba.tdd.tp1.model.book.CrpBook;
import ar.fiuba.tdd.tp1.model.cell.CrPair;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SheetRenameCommandTest {
    private final Book<CrPair, String> book = (new CrpBook<String>()).createSheet("Hoja");

    @Test
    public void excecuteCommand() {
        Command command = new SheetRenameCommand<CrPair, String>("Hoja", "HojaNueva", book);
        command.execute();
        assertFalse(book.existsSheet("Hoja"));
        assertTrue(book.existsSheet("HojaNueva"));
    }

    @Test
    public void excecuteAndUndoCommand() {
        Command command = new SheetRenameCommand<CrPair, String>("Hoja", "HojaNueva", book);
        command.execute();
        command.undo();
        assertTrue(book.existsSheet("Hoja"));
    }

    @Test
    public void undoCommandWithoutExecute() {
        Command command = new SheetRenameCommand<CrPair, String>("Hoja", "HojaNueva", book);
        command.undo();
        assertTrue(book.existsSheet("Hoja"));
    }

    @Test(expected = NullPointerException.class)
    public void commandWithNullParam() {
        new SheetRenameCommand<CrPair, String>(null, "HojaNueva", book);
    }
    
    @Test(expected = InvalidSheetNameException.class)
    public void excecuteCommandWithInvalidName() {
        Command command = new SheetRenameCommand<CrPair, String>("Hoja", "", book);
        command.execute();

    }

    @Test(expected = SheetNotFoundException.class)
    public void excecuteCommandAndExistingSheetFound() throws InvalidCellIdentifierException {
        Command command = new SheetRenameCommand<CrPair, String>("Hojita", "HojaNueva", book);
        command.execute();
    }
}
