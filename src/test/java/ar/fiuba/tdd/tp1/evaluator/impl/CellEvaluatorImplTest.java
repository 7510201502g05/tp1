package ar.fiuba.tdd.tp1.evaluator.impl;

import ar.fiuba.tdd.tp1.evaluator.CellEvaluator;
import ar.fiuba.tdd.tp1.evaluator.CellEvaluatorException;
import ar.fiuba.tdd.tp1.exceptions.model.InvalidCellIdentifierException;
import ar.fiuba.tdd.tp1.parser.Parser;
import ar.fiuba.tdd.tp1.parser.ParserBuilder;
import ar.fiuba.tdd.tp1.parser.expression.Expression;
import ar.fiuba.tdd.tp1.parser.impl.RangePairParserBuilder;
import ar.fiuba.tdd.tp1.parser.impl.StandardExcelParserBuilder;
import ar.fiuba.tdd.tp1.spreadsheet.CellVarIdAccessor;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class CellEvaluatorImplTest {
    private static final Double DELTA = 0.0000000001;

    private CellVarIdAccessor<String> cellVarIdAccessor = new CellVarIdAccessor<String>() {
        @Override
        public String getCellRawValue(String cellId) {
            Map<String, String> vars = new HashMap<String, String>();
            vars.put("hoja1!A23", "1.5");
            vars.put("hoja2!B24", "23.687");
            vars.put("hoja3!C17", "hoja4!D3 + 9.98"); // -7.7844
            vars.put("hoja4!D3", "-17.7644");
            vars.put("hoja5!E6", "hoja3!C17 + hoja2!B24 - hoja1!A23"); // 14.4026

            vars.put("default!A1", "2.25");
            vars.put("default!A2", "= A1 + 0.75"); // 3
            vars.put("default!CB34", "= A2 - 0.5"); // 2.5
            vars.put("default!C6", "4.66");
            vars.put("default!B5", "= hoja1!A23 - A2 + 1.5 + C6"); //4.66 

            vars.put("default!G1", "1.0");
            vars.put("default!G2", "2.55");
            vars.put("default!G3", "5.0");

            return vars.get(cellId);
        }
    };

    @Test
    public void testGetCellDoubleValueOfExpressionWithNoVars() throws CellEvaluatorException {
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        Double doubleValue = new Double(2.24);
        String toParse = doubleValue.toString();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(doubleValue, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithOneVar() throws CellEvaluatorException,
            NumberFormatException, InvalidCellIdentifierException {
        String cellVar = "hoja1!A23";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;
        Double expected = Double.parseDouble(cellAccessor.getCellRawValue(cellVar));

        String toParse = cellVar;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithOneVarAndOneAddition() throws CellEvaluatorException,
            NumberFormatException, InvalidCellIdentifierException {
        String cellVar = "hoja1!A23";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;
        Double expected = Double.parseDouble(cellAccessor.getCellRawValue(cellVar));
        Double addValue = 12.56;
        expected += addValue;

        String toParse = "" + cellVar + " + " + addValue.toString();
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithOneVarAndOneSubs() throws CellEvaluatorException,
            NumberFormatException, InvalidCellIdentifierException {
        String cellVar = "hoja1!A23";

        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;
        Double expected = Double.parseDouble(cellAccessor.getCellRawValue(cellVar));
        Double addValue = 12.56;
        expected -= addValue;

        String toParse = "" + cellVar + " - " + addValue.toString();
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithTwoVarsAddition() throws CellEvaluatorException,
            NumberFormatException, InvalidCellIdentifierException {
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "hoja1!A23";
        Double d1 = Double.parseDouble(cellAccessor.getCellRawValue(cellVar1));

        String cellVar2 = "hoja2!B24";
        Double d2 = Double.parseDouble(cellAccessor.getCellRawValue(cellVar2));

        Double expected = d1 + d2;

        String toParse = "" + cellVar1 + " + " + cellVar2;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithTwoVarsAdditionWithNegativeValue()
            throws CellEvaluatorException, NumberFormatException, InvalidCellIdentifierException {
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "hoja1!A23";
        Double d1 = Double.parseDouble(cellAccessor.getCellRawValue(cellVar1));

        String cellVar2 = "hoja4!D3";
        Double d2 = Double.parseDouble(cellAccessor.getCellRawValue(cellVar2));

        Double expected = d1 + d2;

        String toParse = "" + cellVar1 + " + " + cellVar2;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithSimpleRecursion() throws CellEvaluatorException {
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "hoja3!C17";

        Double expected = -17.7644 + 9.98;

        String toParse = "" + cellVar1;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWithIntermediateRecursion() throws CellEvaluatorException {
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "hoja5!E6";

        Double expected = 14.4026;

        String toParse = "" + cellVar1;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueUsingSheetReferences() throws CellEvaluatorException {
        String sheetDefaultName = "default";
        CellEvaluator cellEvaluator = new CellEvaluatorImpl(sheetDefaultName);
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "CB34";

        Double expected = 2.5;

        String toParse = "" + cellVar1;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfReferencedAndUnreferencedVariables()
            throws CellEvaluatorException {
        String sheetDefaultName = "default";
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        cellEvaluator.defSheetName(sheetDefaultName);
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        String cellVar1 = "B5";

        Double expected = 4.66;

        String toParse = "" + cellVar1;
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

    @Test
    public void testGetCellDoubleValueOfWeirdFormula() throws CellEvaluatorException {
        String sheetDefaultName = "default";
        CellEvaluator cellEvaluator = new CellEvaluatorImpl();
        cellEvaluator.defSheetName(sheetDefaultName);
        CellVarIdAccessor<String> cellAccessor = cellVarIdAccessor;

        Double expected = 4.6 - 5.1 + 16.234;

        String toParse = "= 4.6 -    5.1 + 16.234";
        ParserBuilder parserBuilder = new StandardExcelParserBuilder();
        Double value = cellEvaluator.getCellValueAsDouble(toParse, cellAccessor, parserBuilder);

        assertEquals(expected, value, DELTA);
    }

}
